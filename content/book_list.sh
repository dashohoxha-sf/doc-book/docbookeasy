#!/bin/bash

### Refresh the content of books/book_list with the books
### that are already in the system.

### go to this dir
cd $(dirname $0)

### empty the book_list file
echo "" > books/book_list

### use a custom catalog file
. catalog.sh

### add lines for all the books and languages
books=$(ls books/xml/)
for book_id in $books  #for each book
do
  # get a list of languages in which the book is available
  langs=$(ls books/xml/$book_id/)

  for lng in $langs  #for each language
  do
    # get the title of the book
    xml_file="books/xml/$book_id/$lng/$book_id.xml"
    book_title=$(xsltproc xslt/common/get-title.xsl $xml_file)

    # add a line for the book in the book_list
    echo "$book_id:$lng:$book_title" >> books/book_list

  done #langs
done #books
