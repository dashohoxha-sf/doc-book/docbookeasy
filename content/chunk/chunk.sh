#!/bin/bash
### Converts a DocBook file into docbook chunk files according to
### chapters, sections, etc. These chunk files are included to
### each-other by <xi:include href=... /> .
### The output is placed in the directory explode/tmp/book_id/

### go to the 'content/' dir
cd $(dirname $0)/..

### check parameters
if [ "$1" = "" ]
then
  echo "
Usage: $0 file.xml [doc-id] [lng]

Converts a DocBook file into docbook chunk files according to
chapters, sections, etc. These chunk files are included to
each-other by <xi:include href=... /> .
The output is placed in the directory 'explode/tmp/book_id/' .

Param file.xml is the DocBook document (book, article, etc.) to be chunked.

Param doc-id is the id of the document; if missing, it is taken
      from the attribute 'id' of the root element of file.xml.

Param lng can be: en, en_US, en_US.UTF-8, sq_AL, etc.; if missing it is
      taken from the attribute 'lang' of file.xml (default is en).

Important: the path of file.xml should be either absolute,
           or relative to the 'content/' directory.
"
  exit 1
fi

### get parameters
original_xml_file=$1
book_id=$2
lng=$3

### check that the original_xml_file does exist
if [ ! -f $original_xml_file ]
then
  echo "$0: Error: file not found: $original_xml_file"
  echo "Important: the path of the file.xml should be either absolute,"
  echo "           or relative to the 'content/' directory."
  exit 2
fi

### use a custom catalog file
. catalog.sh

### if book_id and lng are not given, get them from the attributes
### id and lang of the root element of the original xml file
if [ "$book_id" = "" ]
then
  book_id=$(xsltproc xslt/common/get-id.xsl $original_xml_file)
  lng=$(xsltproc xslt/common/get-lang.xsl $original_xml_file)
  lng=${lng:-en}  #default is english
fi

### if book_id is still undefined, stop processing
if [ "$book_id" = "" ]
then
  echo "$0: Error: book_id is undefined."
  exit 3;
fi

echo "Chunking '$book_id/$lng' $original_xml_file"

tmp_dir=chunk/tmp/$book_id/$lng
xml_file=$tmp_dir/$book_id.$lng.xml
rm -rf $tmp_dir
mkdir -p $tmp_dir

### pre-process for extracting <![CDATA[...]]> etc.
cd chunk/
if   [ "${original_xml_file:0:1}" = "/" ]  # it is an absolute path
then preprocess_xml_file=$original_xml_file
else preprocess_xml_file=../$original_xml_file
fi
./pre_process.php tmp/$book_id/$lng $book_id.$lng.xml $preprocess_xml_file
cd ..

### preprocess the xml_file to make it satisfy the DocBookEasy conventions
### (e.g. <sectX> tags are replaced by <section> tags)
xsltproc -o $xml_file xslt/chunk/pre-process.xsl $xml_file

### create the docbook chunks of the given document
xsltproc --stringparam base.dir $tmp_dir/ \
         --stringparam root.filename $book_id \
         xslt/chunk/chunk-hier.xsl $xml_file

### post-process the xml chunks in order to put back <![CDATA[...]]> etc.
cd chunk/
./post_process.php tmp/$book_id/$lng
cd ..

### clean intermediate files
rm $tmp_dir/{cdata.txt,comments.txt,$book_id.$lng.xml}
