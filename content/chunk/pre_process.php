#!/usr/bin/php -q
<?php
   /*
    This file is part of DocBookEasy.  DocBookEasy is a web application
    that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

    DocBookEasy is free software; you can redistribute it and/or modify
    it under the  terms of the GNU General  Public License as published
    by the Free  Software Foundation; either version 2  of the License,
    or (at your option) any later version.

    DocBookEasy is distributed in the  hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
    MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
    General Public License for more details.

    You should have  received a copy of the  GNU General Public License
    along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA
   */

   /**
    * This script replaces <![CDATA[...]]> by <cdata>nr</cdata>
    * and <!--...--> by <comment>nr</comment> in the xml file
    * before it is exploded. 
    * The stripped cdata and comments are saved in files so that 
    * they can be placed back later in the exploded XML chunks.
    */

if ($argc < 2)
  {
    print "Usage: $argv[0] output_dir output_file.xml input_file.xml \n";
    exit(1);
  }
$output_dir = $argv[1];
$output_file_xml = $argv[2];
$input_file_xml = $argv[3];

//get the contents of the file
$fcontents = file_get_contents($input_file_xml);

//replace <![CDATA[...]]> by <cdata>x</cdata>
preg_match_all('#<!\[CDATA\[.*?]]>#s', $fcontents, $matches);
$arr_cdata = $matches[0];
for ($i=0; $i < sizeof($arr_cdata); $i++)
  {
    $fcontents = str_replace($arr_cdata[$i], "<cdata>$i</cdata>", $fcontents);
  }

//replace <!--...--> by <comment>x</comment>
preg_match_all('#<!--.*?-->#s', $fcontents, $matches);
$arr_comments = $matches[0];
for ($i=0; $i < sizeof($arr_comments); $i++)
  {
    $fcontents = str_replace($arr_comments[$i], "<comment>$i</comment>", $fcontents);
  }

//write the modified xml file
$fp = fopen("$output_dir/$output_file_xml", 'w');
fputs($fp, $fcontents);
fclose($fp);

//save arrays for the post-processing script
$fp = fopen("$output_dir/cdata.txt", 'w');
fputs($fp, serialize($arr_cdata));
fclose($fp);
$fp = fopen("$output_dir/comments.txt", 'w');
fputs($fp, serialize($arr_comments));
fclose($fp);
?>