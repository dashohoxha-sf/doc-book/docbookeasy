#!/bin/bash
### Concatenate all the chunks and create a single xml file.
### Can concatenate either the public or the workspace copy of the book.

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book-id [lng] [books_path]"
  echo "'books_path' is either 'books' or 'workspace'"
  exit 1;
fi

book_id=$1
lng=${2:-en}
books=${3:-'books'}

### create a temporary directory
tmp_dir=tmp/$book_id
rm -rf $tmp_dir
mkdir -p $tmp_dir

### copy xml chunks to $tmp_dir
book_dir=../$books/xml/$book_id/$lng
cp -rf $book_dir/* $tmp_dir/

### preprocess xml chunks before imploding (extracting <!-- . . . --> etc.)
./pre_process.php $tmp_dir

### concatenating the xml chunks
echo "Concatenating the chunks of '$book_id/$lng' to $(dirname $0)/$tmp_dir.xml"
xmllint --xinclude --format --output $tmp_dir.xml $tmp_dir/$book_id.xml

### postprocess the imploded xml file (put back <![CDATA[...]]> etc.)
./post_process.php $tmp_dir

### clean the temporary directory
rm -rf $tmp_dir
