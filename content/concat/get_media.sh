#!/bin/bash
### Get all the media files of the book in a separate directory.
### Can use either the public or the workspace copy of the book.

### go to this dir
cd $(dirname $0)

### get the parameters
if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng] [books_path]

'books_path' is either 'books' or 'workspace' (default 'books').
Puts media files of the given book (and language) at:
  $(dirname $0)/tmp/book_id.media.tgz 
If no media file is found, then the tgz file is not created.
"
  exit 1;
fi
book_id=$1
lng=${2:-en}
books=${3:-'books'}

### set variables
script_dir=$(pwd)
book_dir=$(dirname $script_dir)/$books/xml/$book_id/$lng
tmp_media_tgz=$script_dir/tmp/$book_id.media.tgz

### create tmp/ dir
mkdir -p tmp/

### get all the media files from the book directory
cd $book_dir
find .    \( -name .svn -prune \)  \
       -o \( -type f -a ! -name '*.xml' -a ! -name '*.txt' -print \)  \
  | xargs tar cfz $tmp_media_tgz  2> /dev/null

### set proper ownership
cd $script_dir
