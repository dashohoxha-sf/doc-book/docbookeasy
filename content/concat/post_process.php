#!/usr/bin/php -q
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

   /**
    * This script replaces <comment>nr</comment> by the corresponding 
    * <!--...--> in the imploded xml file that is passed as argument.
    * <!--...--> are read from files where they have been saved before
    * by the script implode_strip_cdata.php
    */

if ($argc < 2)
  {
    print "Usage: $argv[0] book_dir \n";
    exit(1);
  }
$dir = $argv[1];

//get the contents of the file
$fname = $dir.'.xml';
$fcontents = file_get_contents($fname);

//replace <cdata>x</cdata> by the corresponding <![CDATA[...]]>
//$arr_cdata = unserialize(file_get_contents("$dir/cdata.txt"));
//for ($i=0; $i < sizeof($arr_cdata); $i++)
//  {
//    $cdata = $arr_cdata[$i];
//    $fcontents = str_replace("<cdata>$i</cdata>", $cdata, $fcontents);
//  }

//replace <comment>x</comment> by the corresponding <!--...-->
$arr_comments = unserialize(file_get_contents("$dir/comments.txt"));
for ($i=0; $i < sizeof($arr_comments); $i++)
  {
    $fcontents = str_replace("<comment>$i</comment>", $arr_comments[$i], $fcontents);
  }

//write back the modified xml file
$fp = fopen($fname, 'w');
fputs($fp, $fcontents);
fclose($fp);
?>
