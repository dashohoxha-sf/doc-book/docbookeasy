#!/bin/bash
### make all the downloadable files, for all the books,
### all the languages and all the formats

### go to this dir
cd $(dirname $0)

books=$(ls ../books/xml/)
for book_id in $books  #for each book
do
  # get a list of languages in which the book is available
  langs=$(ls ../books/xml/$book_id/)

  for lng in $langs  #for each language
  do
    xml_dir=../books/xml/$book_id/$lng/
    xml_download=tar_gz/$book_id/$lng/$book_id.$lng.xml.tar.gz

    # make the downloads only if there is any modification 
    # after the last time that downloads were generated
    find_result=$(find $xml_dir -newer $xml_download 2>&1)
    if [ "${find_result:0:1}" != "" ]
    then 
      ./make-downloads.sh $book_id $lng
    fi
  done #langs
done #books
