#!/bin/bash
### Generate the formats and the downloads for the given book_id and lng.
### The different generated formats are stored at: 'formats/$book_id/$lng/'
### and the tgz files are stored at: 'tar_gz/$book_id/$lng/'.

### go to this dir
cd $(dirname $0)

### get parameters
if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  exit 1;
fi
book_id=$1
lng=${2:-en}

### make directory
formats=formats/$book_id/$lng
rm -rf $formats
mkdir -p $formats

### link xml directory to formats/
echo "==> $formats/xml/"
rm -f $formats/xml
xml_dir=../books/xml/$book_id/$lng/
ln -s ../../../$xml_dir $formats/xml

### generate different formats

echo "==> $formats/html/"
./xmlto.sh $book_id html $lng
./docbook2.sh $book_id html $lng

echo "==> $formats/html1/"
./xmlto.sh $book_id html1 $lng
./docbook2.sh $book_id html1 $lng

echo "==> $formats/pdf/"
#./xmlto.sh $book_id pdf $lng
#./docbook2.sh $book_id pdf $lng
./dblatex.sh $book_id pdf $lng

#echo "==> $formats/rtf/"
#./docbook2.sh $book_id rtf $lng

echo "==> $formats/txt/"
./xmlto.sh $book_id txt $lng
#./docbook2.sh $book_id txt $lng

#echo "==> $formats/ps/"
#./xmlto.sh $book_id ps $lng
#./docbook2.sh $book_id ps $lng
#./dblatex.sh $book_id ps $lng

echo "==> $formats/tex/"
#./docbook2.sh $book_id tex $lng
./dblatex.sh $book_id tex $lng

### Generating *.tar.gz files in $tar_gz
tar_gz=tar_gz/$book_id/$lng
rm -rf $tar_gz
mkdir -p $tar_gz
echo
echo "=============================================================="
echo "== Generating *.tar.gz files in '$tar_gz/'"
echo "=============================================================="
for fmt in $(ls $formats)
do
  echo "--> $tar_gz/$book.$fmt.tar.gz"
  tar cfz $tar_gz/$book_id.$lng.$fmt.tar.gz -C $formats/$fmt/  . 
done
