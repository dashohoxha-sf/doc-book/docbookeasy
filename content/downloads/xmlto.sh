#!/bin/bash
### Convert to other formats using 'xmlto' (XML docbook + XSL stylesheets).
### Has parameters: book_id, format, lng.
### The input file is 'formats/$book_id/$lng/xml/$book_id.$lng.xml'.
### Media files of the book should be in: 'formats/$book_id/$lng/media/'.
### The output is placed in directory: 'formats/$book_id/$lng/$format/'.

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} book_id [ html | html1 | pdf | ps | txt ] [lng]"
  exit 1
}

if [ "$1" = "" ]; then usage; fi
echo "--> $0 $1 $2 $3"

### get parameters
book_id=$1
format=$2
lng=${3:-en}

### set variables
book_xml=formats/$book_id/$lng/xml/$book_id.xml
xml_file=$book_xml
#media=formats/$book_id/$lng/media
output=formats/$book_id/$lng/$format
tmp=formats/tmp

### make directories
mkdir -p $output/
mkdir -p $tmp/

### generate the requested format
case "$format" in
  html )
     outdir=$output/$book_id.$lng.xmlto/
     mkdir $outdir
     stylesheet=xsl-stylesheets/xhtml-chunk.xsl
     xmlto xhtml -vv -o $outdir -x $stylesheet $xml_file

     ### create a link to the media files of the book
     #ln -s ../../media $output
     ;;

  html1 )
     stylesheet=xsl-stylesheets/xhtml-docbook.xsl
     xmlto xhtml-nochunks -vv -o $tmp/ -x $stylesheet $xml_file
     mv $tmp/$book_id.html $output/$book_id.$lng.xmlto.html

     ### create a link to the media files of the book
     #ln -s ../../media $output/
     ;;

  pdf )
     stylesheet=xsl-stylesheets/fo-docbook.xsl
     xmlto pdf -v -o $tmp/ -x $stylesheet $xml_file
     mv $tmp/$book_id.pdf $output/$book_id.$lng.xmlto.pdf
     ;;

  ps  ) 
     stylesheet=xsl-stylesheets/fo-docbook.xsl
     xmlto ps -v -o $tmp/ -x $stylesheet $xml_file
     mv $tmp/$book_id.ps $output/$book_id.$lng.xmlto.ps 
     ;;

  txt )
     xmlto txt -v -o $tmp/  $xml_file
     mv $tmp/$book_id.txt $output/$book_id.$lng.xmlto.txt
     #ln -s ../../media $outdir
     ;;

  *   ) usage ;;
esac

### clean up
rm -rf $tmp/
