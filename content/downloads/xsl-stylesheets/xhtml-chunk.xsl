<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- Customization of the xhtml chunk stylesheets. -->

<!--
<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/docbook.xsl"/>
-->
<xsl:import href="xhtml-docbook.xsl"/>
<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk-common.xsl"/>
<xsl:include href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk-code.xsl"/>


<xsl:param name="chunker.output.encoding" select="'UTF-8'"/>
<xsl:param name="chunker.output.standalone" select="'no'"/>
<xsl:param name="chunker.output.indent" select="'yes'"/>

<xsl:param name="root.filename" select="'index'"/>
<xsl:param name="use.id.as.filename" select="1"/>
<xsl:param name="html.ext" select="'.xhtml'"/>
<xsl:param name="chunk.first.sections" select="1"></xsl:param>
<xsl:param name="chunk.fast" select="1"/>
<!-- <xsl:param name="chunk.quietly" select="1"/> -->


</xsl:stylesheet>
