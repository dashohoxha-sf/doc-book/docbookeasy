#!/bin/bash
### enable email notifications for the given book

### go to the this directory
cd $(dirname $0)

### get the parameters
if [ $# -lt 2 ]
then
  echo "Usage: $0 (enable | disable | show) book_id [lng]"
  exit 1
fi
action=$1
book_id=$2
lng=${3:-en}

### get some variables
book_dir=../workspace/xml/$book_id/$lng/
svn_url=$(svn info $book_dir | grep 'Repository Root: ' | cut -d' ' -f3)
url_type=${svn_url%://*}

### check that the repository is a local one (file://...)
if [ "$url_type" != "file" ]
then 
  echo "$0: Repository of $book_id/$lng is remote."
  exit 1
fi

### get the post commit file
repo_dir=${svn_url#file://}
post_commit_file=$repo_dir/hooks/post-commit

function enable
{
  ### get ADMIN_EMAIL
  .  ../../docbookeasy.conf

  ### create a post-commit hook in the repository
  cp -f post-commit.tmpl $post_commit_file
  cat <<EOF >> $post_commit_file
$(pwd)/commit-email.pl "\$REPOS" "\$REV" \\
    --from $ADMIN_EMAIL -s '[docbookeasy]' $ADMIN_EMAIL

EOF
}

function disable
{
  rm -f $post_commit_file
}

function show
{
  if [ -f $post_commit_file ]
  then echo "enabled"
  else echo "disabled"
  fi
}

case $action in
  enable  )  enable ;;
  disable )  disable ;;
  show    )  show ;;
  *       )  usage ;;
esac
