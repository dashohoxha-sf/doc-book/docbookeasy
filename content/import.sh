#!/bin/bash
### Import a DocBook document in the DocBookEasy system.

### go to this dir
cd $(dirname $0)

### get the parameters
if [ "$1" = "" ]
then
  echo "
Usage: $0 file.xml

The parameter 'file.xml' is the DocBook document to be imported.
The path of the file 'file.xml' should be either absolute, or relative
to the 'content/' directory.
"
  exit 1
fi
xml_file=$1

### check that the xml_file does exist
if [ ! -f $xml_file ]
then
  echo "$0: Error: file not found: $xml_file
    The path of the files 'file.xml' and 'media-files.tgz' should be 
    either absolute, or relative to the 'content/' directory."
  exit 2
fi

### use a custom catalog file
. catalog.sh

### get book_id and lng from the attributes id and lang 
### of the root element (book or article) of the xml file
book_id=$(xsltproc xslt/common/get-id.xsl $xml_file)
lng=$(xsltproc xslt/common/get-lang.xsl $xml_file)
lng=${lng:-en}  #default is english

### if book_id is undefined, stop processing
if [ "$book_id" = "" ]
then
  echo "$0: Error: book_id is undefined."
  exit 3;
fi

### clean first, in case there is an existing copy
./clean.sh $book_id $lng

### import the book to a svn repository
svn_repo=svn/$book_id/$lng
import/svn_import.sh $xml_file $svn_repo

### checkout a book from the svn repository
svn_url=file://$(pwd)/$svn_repo
import/svn_checkout.sh $svn_url $book_id $lng

### done
echo "----------"
