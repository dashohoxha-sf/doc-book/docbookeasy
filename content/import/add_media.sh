#!/bin/bash
### add media files to the book

### go to the content/ directory
cd $(dirname $0)
cd ..

### get the parameters
if [ $# -lt 2 ]
then
  echo "
Usage: $0 media-files.tgz book_id [lng]

Add media files to the given book.

The parameter 'media-files.tgz' contains the media files of the book,
organized in directories and subdirectories according to the chunks
and subchunks to which they belong.

The parameter lng can be: en, en_US, en_US.UTF-8, sq_AL, etc. (default is en).

Note: the path of the file 'media-files.tgz' should be either absolute,
      or relative to the 'content/' directory.
"
  exit 1
fi
media_files=$1
book_id=$2
lng=${3:-en}

### check that the media file exists
if [ ! -f $media_files ]
then
  echo "$0: Cannot find $media_files"
  exit 1
fi

### copy media files to the workspace directory of the book
tar xfz $media_files -C workspace/xml/$book_id/$lng

### get a list of all the media files
media_file_list=$(tar tvfz $media_files | gawk '{print $6}' | sed -e '\|/$|d')

### check that the $media_file_list is not empty
if [ "$media_file_list" = "" ]; then exit;fi

### add and commit to svn all media files
current_dir=$(pwd)
cd workspace/xml/$book_id/$lng
svn add $media_file_list
svn commit -m 'adding media files' $media_file_list
cd $current_dir

### update the public dir
svn update books/xml/$book_id/$lng
