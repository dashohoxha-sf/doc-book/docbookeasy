#!/bin/bash
### create an initial *.state.txt file for each chunk in the workspace

### go to the content/ directory
cd $(dirname $0)
cd ..

### get the parameters
if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]
Create an initial *.state.txt file for each chunk in the workspace.
If the parameter 'lng' is not given, the default is 'en'.
"
  exit 1
fi
book_id=$1
lng=${2:-en}

### get ADMIN_EMAIL from docbookeasy.conf
.  ../docbookeasy.conf
email=$ADMIN_EMAIL
user=$(whoami)

### create a state file *.state.txt for each chunk in the workspace
echo "Creating files *.state.txt in workspace/xml/$book_id/$lng/"
timestamp=$(date +%s)
chunk_list=$(find workspace/xml/$book_id/$lng/ -name '*.xml')
for chunk in $chunk_list
do
  state_file=${chunk/%.xml/.state.txt}
  echo "unlocked::::" > $state_file
  echo "imported:$user:$email:$timestamp" >> $state_file
done
