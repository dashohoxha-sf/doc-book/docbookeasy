#!/bin/bash
### checkout a book from the svn repository

### go to the content/ directory
cd $(dirname $0)
cd ..

### get the parameters
if [ "$1" = "" ]
then
  echo "
Usage: $0 svn_url [book_id] [lng]

Checkout a book from the given svn repository (which can be
local or remote). It is checked out in the public space and 
in the workspace, and the xhtml files are created as well.

The parameter 'svn_url' is the url of the svn repository
of the document, which can be local or remote.
If 'book_id' is not given, then 'book_id' and 'lng' are
extracted from 'svn_url'.
"
  exit 1
fi

svn_url=$1
book_id=$2
lng=$3

if [ "$book_id" = "" ]
then
  lng=$(basename $svn_url)
  book_id=$(basename $(dirname $svn_url))
fi

### checkout into books/xml/
echo "Checking out to books/xml/$book_id/$lng/"
svn checkout -q $svn_url/trunk books/xml/$book_id/$lng/

### checkout into workspace/xml/
if [ ! -d workspace/xml/$book_id/ ]
then
  mkdir -p workspace/xml/$book_id/
fi
echo "Checking out to workspace/xml/$book_id/$lng/"
svn checkout -q $svn_url/trunk workspace/xml/$book_id/$lng/

### create the xhtml files
echo "Creating xhtml files in books/xhtml/$book_id/$lng/"
xhtml/xhtml-allchunks.sh $book_id $lng 'books'
echo "Creating xhtml files in workspace/xhtml/$book_id/$lng/"
xhtml/xhtml-allchunks.sh $book_id $lng 'workspace'

### create an initial *.state.txt file for each chunk in the workspace
import/init_state.sh $book_id $lng

### use a custom catalog file
. catalog.sh

### append book to the book_list
book_title=$(xsltproc xslt/common/get-title.xsl $xml_file)
echo "$book_id:$lng:$book_title" >> books/book_list
