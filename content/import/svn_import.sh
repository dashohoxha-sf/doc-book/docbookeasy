#!/bin/bash
### import the book to the given svn repository

### go to the content/ directory
cd $(dirname $0)
cd ..

### get the parameters
if [ "$1" = "" ]
then
  echo "
Usage: $0 xml_file [svn_repo]

Import the 'xml_file' document to a svn repository.

The parameter 'svn_repo' is the path where the svn repository is created. 
If it is missing then a default path of 'svn/book_id/lng/' is used.

Note: The path of the 'xml_file' should be either absolute, or relative
      to the 'content/' directory. The same applies to 'svn_repo'.
"
  exit 1
fi
xml_file=$1
svn_repo=$2

### use a custom catalog file
. catalog.sh

### get book_id and lng from the xml file
book_id=$(xsltproc xslt/common/get-id.xsl $xml_file)
lng=$(xsltproc xslt/common/get-lang.xsl $xml_file)
lng=${lng:-en}  #default is english

### if the parameter repository is not given, then use a local directory
if [ "$svn_repo" = "" ]; then svn_repo=svn/$book_id/$lng; fi

### get the absolute path of svn_repo
current_dir=$(pwd)
mkdir -p $svn_repo
cd $svn_repo
abs_svn_repo=$(pwd)
cd $current_dir

### create a local svn repository and get its url
svnadmin create $abs_svn_repo
svn_url=file://$abs_svn_repo

### convert the xml file to a chunked format
chunk/chunk.sh $xml_file $book_id $lng

### import into svn
echo "Importing into:"
echo $svn_url/trunk
svn import -q chunk/tmp/$book_id/$lng $svn_url/trunk -m ""
rm -rf chunk/tmp/$book_id/$lng

### make a directory for tags in the svn 
svn mkdir -m 'directory for tags' $svn_url/tags
