#!/bin/bash
### creates the content of the folders books/, books_html/,
### workspace/books/, etc. by exploding the initial xml files

### go to this dir
cd $(dirname $0)

### erase any existing books
./clean.sh all

### create some directories
mkdir -p books/svn workspace initial_xml/uploaded

#book_list="docbookeasy
#           slackware_router
#           gateway_server
#           tools_and_apps
#           install_exim
#           linux_server_config
#           lcg_install
#           inima_soft_server
#           inima_gateway_server"
book_list="docbookeasy slackware_router"

### import books in the system
for book_id in $book_list
do
  ### create a local svn repository and import it there 
  xml_file=initial_xml/${book_id}_en.xml
  svn_repo=../svn_repo/$book_id/en
  import/svn_import.sh $xml_file $svn_repo

  ### checkout from a svn repository
  svn_url=file://$(dirname $(pwd))/svn_repo/$book_id/en
  import/svn_checkout.sh $svn_url $book_id en
done

### add media files
media_files=initial_xml/slackware_router.media.tgz
import/add_media.sh $media_files slackware_router en

