#!/bin/bash
### Creates the xhtml files for each chunk

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book-id [lng] [books-path]"
  echo "'books-path' is either 'books' or 'workspace'"
  exit 1;
fi

book_id=$1
lng=${2:-en}
books=${3:-"books"}

### use a custom catalog file
. ../catalog.sh

xmldoc=../$books/xml/$book_id/$lng/$book_id.xml
xhtml_path=../$books/xhtml/$book_id/$lng

rm -rf $xhtml_path/
mkdir -p $xhtml_path/

### create the xhtml files for each chunk
xsltproc --stringparam base.dir $xhtml_path/ \
         --stringparam root.filename $book_id \
         --xinclude \
         xslt/xhtml/xhtml-hier.xsl $xmldoc

