#!/bin/bash
### Creates the xhtml file for the given xml chunk, for its ancestors
### and for its siblings. (An attempt to make xhtml regeneration a bit
### more efficient by generating only those parts that need to be generated.)

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book-id chunk [lng] [books-path]"
  echo "'books-path' is either 'books' or 'workspace'"
  exit 1;
fi

book_id=$1
chunk=$2
lng=${3:-en}
books=${4:-"books"}

### use a custom catalog file
. ../catalog.sh

chunk_id=$(basename $chunk)
chunk_dir=$(dirname $chunk)
xmldoc=../$books/xml/$book_id/$lng/$book_id.xml
#xhtml_path=../$books/xhtml/$book_id/$lng
xhtml_path=test-xhtml-1/  #test

rm -rf $xhtml_path/
mkdir -p $xhtml_path/

### create the xhtml files for each chunk
xsltproc --stringparam base.dir $xhtml_path/ \
         --stringparam root.filename $book_id \
         --stringparam chunk.id $chunk_id \
         --xinclude \
         xslt/xhtml/xhtml-onechunk.xsl $xmldoc

