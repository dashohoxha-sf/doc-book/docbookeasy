#!/bin/bash

### go to the root dir
cd $(dirname $0)/..

### modify the php scripts in 'content/' by changing
### the first line to the correct path of the php
php=$(which php)
find content/ -name '*.php' | xargs sed "1 c #\!$php -q" -i

### modify the path of docbookeasy in 'catalog-docbookeasy',
### in 'catalog.xml' and in 'content/catalog.sh'
dir=$(pwd)
sed -i -e "s#/var/www/docbookeasy#$dir#" catalog-docbookeasy
sed -i -e "s#/var/www/docbookeasy#$dir#" catalog.xml
sed -i -e "s#/var/www/docbookeasy#$dir#" content/catalog.sh

### modify search/*.sh by changing $swishe; modify docbookeasy.conf as well
swishe=$(which swish-e)
sed -i "/swishe=/ c swishe=$swishe" search/*.sh
sed -i "/SWISH_E=/ c SWISH_E='$swishe'" docbookeasy.conf

#### modify docbookeasy.conf by setting the propper path for REFDBC
#refdbc=$(which refdbc)
#sed -i "/REFDBC=/ c REFDBC='$refdbc'" docbookeasy.conf

### modify content/downloads/dblatex.sh by changing $dblatexpath
dblatex=$(which dblatex)
dblatex_file=content/downloads/dblatex.sh
sed -i "/dblatexpath=/ c dblatexpath=$dblatex" $dblatex_file
