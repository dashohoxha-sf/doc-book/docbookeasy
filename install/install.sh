#!/bin/bash

### go to the root directory
cd $(dirname $0)
cd ..

### configure the application files
install/configure.sh

### make the translation files
l10n/msgfmt-all.sh

### set 'admin' as the default superuser password
install/set_su_passwd.sh admin

### prepare the database for webnotes
install/webnotes.sh

#### add a user for docbookeasy in refdb
#install/refdb.sh

### generate the content files from initial xml files
content/clean.sh all
content/make-content.sh

### index the content for searching
search/make_index.sh

### run make-all-downloads.sh
nice content/downloads/make-all-downloads.sh

#### enable email notifications on commit
#content/email-notifications/email-notifications.sh enable
