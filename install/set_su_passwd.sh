#!/bin/bash
### set the password of the superuser

### go to the root dir
cd $(dirname $0)/..

### get the superuser password from the first parameter
su_passwd=$1

### if it was not given, get it interactivly
if [ "$su_passwd" = "" ]
then
  echo "$0: Setting password for 'superuser' of DocBookEasy."
  echo -n "$0: Enter the password of 'superuser' [admin]: "
  read su_passwd

  # if no passwd was entered, set 'admin' as the default passwd
  su_passwd=${su_passwd:-admin}
fi

### set the superuser password
mkdir -p .su
crypted_passwd=$(openssl passwd -1 $su_passwd)
echo $crypted_passwd > .su/passwd
