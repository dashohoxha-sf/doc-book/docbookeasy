#!/bin/bash
### configure sudo to allow apache to run with DATA_OWNER permissions

### check that it is called  by root
if [ "$(whoami)" != "root" ]
then
  echo "$0: should be called by root"
  exit 1
fi

### go to this directory
cd $(dirname $0)

### get the variable DATA_OWNER from the configuration file
. ../docbookeasy.conf

### disable requiretty at /etc/sudoers
sed -i '/requiretty/ c #Defaults    requiretty' /etc/sudoers

### get the system user that is used for apache
if    [ -f /etc/redhat-release ]     # RedHat family
then
  apache='apache'
elif  [ -f /etc/debian_version ]     # Debian family
then
  apache='www-data'
elif  [ -f /etc/slackware-version ]  # Slackware
then
  apache='nobody'
else
  apache='nobody'
fi

### modify /etc/sudoers to allow apache to run everything as DATA_OWNER
comment="### allow apache to run everything as $DATA_OWNER without a password"
sed -i "/^$comment/,+2 d" /etc/sudoers
cat <<EOF >> /etc/sudoers
$comment
$apache     ALL = ($DATA_OWNER) NOPASSWD: ALL

EOF

