#!/bin/bash
### create the database used by webnotes

### go to the root dir
cd $(dirname $0)/..

### get the webnotes settings from 'docbookeasy.conf'
. docbookeasy.conf

### check that webnotes are enabled
if [ "$WEBNOTES_ENABLE" = "false" ]; then exit; fi

### get DB parameters
host=$WEBNOTES_DBHOST
user=$WEBNOTES_DBUSER
pass=$WEBNOTES_DBPASS
dbname=$WEBNOTES_DBNAME

### create the database
if [ "$user" = "root" ]
then
  rootpasswd=$pass
else
  rootpasswd=""
  echo "$0: Creating the database webnotes."
  echo "$0: Enter the password of the 'root' user of mysql:"
fi
echo "drop database if exists $dbname ; create database $dbname ;" \
  | mysql --host=$host --user=root --password=$rootpasswd

### create the user
if [ "$user" != "root" ]
then
  echo "Creating the database user, enter the mysql root password:"
  echo "grant all privileges on $dbname.* to $user@localhost 
        identified by '$pass';" \
    | mysql --host=$host --user=root --password
fi

### create the tables
mysql --host=$host --user=$user --password=$pass \
      --database=$dbname < web_app/boxes/webnotes/db/webnotes.sql

