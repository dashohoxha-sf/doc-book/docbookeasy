#!/bin/bash
### update old *.po files with new messages extracted by xgettext.sh

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

dir=$lng/LC_MESSAGES
mv $dir/docbookeasy.po docbookeasy-$lng-old.po
msgmerge --output-file=$dir/docbookeasy.po  docbookeasy-$lng-old.po docbookeasy.po
