<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Displays a list of the current books in the system.
   *
   * @package admin
   * @subpackage addbook
   */
class book_list extends WebObject
{
  function init()
  {
    $this->addSVar('visible', 'false');
  }

  function on_set_visible($event_args)
  {
    $visible = $event_args['visible'];
    $this->setSVar('visible', $visible);
  }

  function on_delete($event_args)
  {
    $book_id = $event_args['book_id'];
    $lng = $event_args['lng'];
    $clean_sh = CONTENT.'clean.sh';
    $output = shell(CONTENT."clean.sh $book_id $lng");
    WebApp::debug_msg("<xmp>$output</xmp>");
  }

  function onRender()
  {
    $this->add_recordsets();
  }

  function add_recordsets()
  {
    global $webPage;

    //the number of columns/chunks
    $nr_cols = 3; 

    //add a recordset for the columns
    $rs_cols = new EditableRS("cols");

    //get the list of books
    $fname = CONTENT.'books/book_list';
    $arr_lines = file($fname);
    $nr_chunks = sizeof($arr_lines) / $nr_cols;
    if (sizeof($arr_lines) % $nr_cols)  $nr_chunks++;
    $arr_chunks = array_chunk($arr_lines, $nr_chunks);

    //create, fill and add a recordset for each column/chunk
    for ($i=0; $i < sizeof($arr_chunks); $i++)
      {
        //create a new recordset for the chunk
        $rs = new EditableRS("books_$i");

        //add the items of the chunk in the recordset
        $chunk = $arr_chunks[$i];
        for ($j=0; $j < sizeof($chunk); $j++)
          {
            $line = $chunk[$j];
            if (trim($line)=='')  continue;

            //get book_id, lng and title of the book
            list($book_id, $lng, $title) = split(':', $line, 3);
            $title = trim($title);

            //add a record for this book
            $rs->addRec(compact('book_id', 'lng', 'title'));
          }

        //add the recordset to the webPage
        $webPage->addRecordset($rs);

        //add a record for this column/chunk
        $rs_cols->addRec(array('nr'=> $i));
      }

    //add the column recordset to the webPage
    $webPage->addRecordset($rs_cols);
  }
}
?>