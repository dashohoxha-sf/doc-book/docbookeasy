<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * The books that the user can administrate.
   * @package admin
   * @subpackage books
   */
class books extends WebObject
{
  function init()
  {
    $arr_book_list = $this->get_book_list_arr();
    $this->addSVar('selected_book', $arr_book_list[0]);
  }

  function on_select($event_args)
  {
    $book_id = $event_args['book_id'];
    $this->setSVar('selected_book', $book_id);
  }

  function onRender()
  {
    $this->add_book_title();
    $this->add_book_list_rs();
  }

  function add_book_title()
  {
    $book_list = CONTENT.'books/book_list';
    $book_id = $this->getSVar('selected_book');

    $lng = WebApp::getSVar('language');
    $book_title = shell("grep '^$book_id:$lng:' $book_list | cut -d: -f3");

    if ($book_title=='')
      {
        $lng = 'en';
        $book_title = shell("grep '^$book_id:$lng:' $book_list | cut -d: -f3");
      }

    if ($book_title=='')
      {
        $book_title = shell("grep '^$book_id:' $book_list | head -n 1 | cut -d: -f3");
      }

    WebApp::addVar('book_title', trim($book_title));    
  }

  function add_book_list_rs()
  {
    //get $arr_books
    $book_list = CONTENT.'books/book_list';
    $books = shell("cat $book_list | cut -d: -f1 | uniq");
    $arr_books = explode("\n", trim($books));

    $rs = new EditableRS('book_list');
    $arr_book_list = $this->get_book_list_arr();
    for ($i=0; $i < sizeof($arr_book_list); $i++)
      {
        $book_id = $arr_book_list[$i];
        if (in_array($book_id, $arr_books))
          {
            $rec = array('id'=>$book_id, 'label'=>$book_id);
            $rs->addRec($rec);
          }
      }
    global $webPage;
    $webPage->addRecordset($rs);
  }

  function get_book_list_arr()
  {
    $record = shell(SCRIPTS.'users/get_user.sh '.USER);
    $record = trim($record);
    $fields = explode(':', $record);
    $book_list =  $fields[4];
    $arr = explode(',', $book_list);

    return $arr;
  }
}
?>