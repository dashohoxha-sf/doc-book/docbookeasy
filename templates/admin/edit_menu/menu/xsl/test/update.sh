#!/bin/bash

id=nontechnical
bookid=non-tech
caption='Non Technical'
#xsl_file=../update_bookid.xsl
xsl_file=../update_caption.xsl
xml_file=../../menu_en.xml

xsltproc  --stringparam id "$id" \
          --stringparam bookid "$bookid" \
          --stringparam caption "$caption" \
          $xsl_file  $xml_file
