<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /** 
   * The $menu_items array contains the items of the tabs1.
   *
   * @package     admin
   */

$menu_items = array();

//modules Menu and Users can be used only by superuser
if (SU=='true')
  {
    $menu_items['addbook'] = T_("Add Book");
    if (USE_MENU=='true')  $menu_items['menu'] = T_("Menu");
    $menu_items['users'] = T_("Users");
  }

//get the list of the books that can be administrated by the user
$record = shell(SCRIPTS."users/get_user.sh ".USER);
$fields = explode(':', $record);
$book_list = trim($fields[4]);

//add the book administration module only if the user 
//can administrate some books (book_list is not empty)
if (!(SU=='true') and $book_list != '')
  {
    $menu_items['books'] = T_("Books");
  }

//if the menu has no items, don't add the last item at all
if (sizeof($menu_items) > 0)
  {
    if (!(SU=='true'))
      {
        $menu_items['user_data'] = T_("My Settings");
      }
  }
?>