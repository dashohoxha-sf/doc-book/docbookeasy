<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Handle (view, select) the previous revisions of the book.
   *
   * @package docbook
   * @subpackage approve
   */
class revisions extends WebObject
{
  function init()
  {
    $this->addSVar('visible', 'false');
    $this->addSVar('tag', '');
    $this->addSVar('revision', '');
    $this->addSVar('date', '');
  }

  function on_show($event_args)
  {
    $this->setSVar('visible', 'true');
  }

  function on_hide($event_args)
  {
    $this->setSVar('visible', 'false');
  }

  function clear()
  {
    $this->setSVar('tag', '');
    $this->setSVar('revision', '');
    $this->setSVar('date', '');
  }

  function on_set_tag($event_args)
  {
    $this->clear();
    $tag = $event_args['tag'];
    $this->setSVar('tag', $tag);
  }

  function on_set_rev($event_args)
  {
    $this->clear();
    $rev = $event_args['rev'];
    $this->setSVar('revision', $rev);
  }

  function on_set_date($event_args)
  {
    $this->clear();
    $date = $event_args['date'];
    $this->setSVar('date', $date);
  }

  function on_save($event_args)
  {
    //write it to content_xml
    $content_xml = file_content_xml();
    $file_revision = $this->get_revision();
    write_file($content_xml, $file_revision);

    //update the cache file content.html
    update_cache();

    //set the status of the node to modified
    set_node_status('modified');

    //clear any selected revision
    $this->clear();
  }

  function onRender()
  {
    $visible = $this->getSVar('visible');
    if ($visible=='true')
      {
        $this->add_rs_tags();
        $this->add_rs_revs();
        $this->add_file_revision();
        $this->add_rev_diff();
      }
  }

  /** Add to webpage a recordset of tags. */
  function add_rs_tags()
  {
    $rs = new EditableRS('tags');

    $url = $this->get_svn_url();
    $output = shell("svn list $url/tags/");

    $rs->addRec(array('id'=>'', 'label'=>''));
    $lines = explode("\n", $output);
    for ($i=0; $i < sizeof($lines); $i++)
      {
        $line = $lines[$i];
        if ($line=='')  continue;
        $tag = substr($line, 0, -1);
        $rs->addRec(array('id'=>$tag, 'label'=>$tag));
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }

  /** Add to webpage a recordset of revs. */
  function add_rs_revs()
  {
    $rs = new EditableRS('revs');

    $content_xml = file_content_xml(WS_BOOKS);
    $output = shell("svn log -q $content_xml");

    $rs->addRec(array('id'=>'', 'label'=>''));
    $lines = explode("\n", $output);
    for ($i=0; $i < sizeof($lines); $i++)
      {
        $line = $lines[$i];
        if (substr($line, 0, 4)=='----')  continue;
        $arr = explode(' ', $line);
        $rev = $arr[0];
        $rev = substr($rev, 1);
        $rs->addRec(array('id'=>$rev, 'label'=>$rev));
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }

  /** add the variable {{file_revision}} */
  function add_file_revision()
  {
    //get the revision of the file
    $content_xml = $this->get_revision();
    if (substr($content_xml, 0, 4)=='svn:')
      {
        WebApp::addVar('file_revision', '');
        return;
      }

    //write it in a temporary file
    $tmpfile = write_tmp_file($content_xml);

    //convert it to html
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $book_path  = WS_BOOKS.$book_id.'/'.$lng.'/';
    $params = "--stringparam book_path \"$book_path\"";
    $xsl_file = XSLT."cache/update_content.xsl";
    $content_html = shell("xsltproc $params $xsl_file $tmpfile");
    unlink($tmpfile);

    //add the variable
    WebApp::addVar('file_revision', $content_html);
  }

  /** return the content.xml at the selected revision (or date, or tag) */
  function get_revision()
  {
    extract($this->getSVars());
    $content_xml = file_content_xml(WS_BOOKS);

    if ($tag != '')
      {
        $url = $this->get_svn_url();
        $node_path = WebApp::getSVar('docbook->node_path');
        $cmd = "svn cat $url/tags/$tag/${node_path}content.xml";
      }
    else if ($revision != '')
      {
        $cmd = "svn cat -r $revision $content_xml";
      }
    else if ($date != '')
      {
        $cmd = "svn cat -r {'$date'} $content_xml";
      }
    else
      {
        $cmd = "svn cat $content_xml";
      }

    $output = shell($cmd);
    return $output;
  }

  /** add {{rev_diff}}, the difference 
   * of the selected revision with the HEAD */
  function add_rev_diff()
  {
    extract($this->getSVars());
    $content_xml = file_content_xml(WS_BOOKS);
    $get_title_xsl = XSLT.'edit/get_content_title.xsl';

    //get the selected revision of the file
    if ($tag != '')
      {
        $url = $this->get_svn_url();
        $node_path = WebApp::getSVar('docbook->node_path');
        $tag_url = "$url/tags/$tag/$node_path".'content.xml';
        $xml_rev = shell("svn cat $tag_url");
        $log = shell("svn log $tag_url");
      }
    else if ($date != '')
      {
        $xml_rev = shell("svn cat $content_xml -r {'$date'}");
        $log = shell("svn log $content_xml -r {'$date'}");
      }
    else if ($revision != '')
      {
        $xml_rev = shell("svn cat $content_xml -r $revision");
        $log = shell("svn log $content_xml -r $revision");
      }
    else
      {
        $xml_rev = shell("svn cat $content_xml -r HEAD");
        $log = shell("svn log $content_xml -r HEAD");
      }
    $file_xml_rev = write_tmp_file($xml_rev);

    //convert to TextWiki the selected revision
    $title = shell("xsltproc $get_title_xsl $file_xml_rev");
    $txt = get_node_content($file_xml_rev, 'text');
    $txt = 'T: '.$title."\n\n".trim($txt)."\n";
    $file_txt_rev = write_tmp_file($txt);

    //convert to TextWiki the working copy
    $title = shell("xsltproc $get_title_xsl $content_xml");
    $txt = get_node_content($content_xml, 'text');
    $txt = 'T: '.$title."\n\n".trim($txt)."\n";
    $file_txt = write_tmp_file($txt);

    //get the difference
    $diff = shell("diff -ubB $file_txt $file_txt_rev | sed '1,2d'");

    //clean the temporary files
    unlink($file_xml_rev);
    unlink($file_txt_rev);
    unlink($file_txt);

    //format diff for propper display in html
    $diff = htmlspecialchars($diff);
    $patterns = array(
                      '/^(@@.*@@)$/m',
                      '/^-(.*)$/m',
                      '/^\+(.*)$/m',
                      '/^ (.*)$/m'
                      );
    $replacements = array(
                          '<div class="line_nr">\\1</div>',
                          '<div class="removed">&nbsp;\\1</div>',
                          '<div class="added">&nbsp;\\1</div>',
                          '<div class="context">&nbsp;\\1</div>'
                          );
    $diff = preg_replace($patterns, $replacements, $diff);

    //format log for propper display in html
    $patterns = array(
                      '/^-----+$/m',
                      '/\n+/'
                      );
    $replacements = array(
                          '',
                          "\n"
                          );
    $log = preg_replace($patterns, $replacements, $log);

    //add variables
    WebApp::addVar('rev_diff', $diff);
    WebApp::addVar('rev_log', $log);
  }

  /** return the URL of the SVN repository: file://path/book_id/lng */
  function get_svn_url()
  {
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $book_dir = WS_BOOKS."$book_id/$lng/";

    $url = shell("svn info $book_dir | sed -n '/^URL: /p'");

    $url = ereg_replace('^URL: ', '', $url);
    $url = ereg_replace("$book_id/$lng/.*", "$book_id/$lng", $url);

    return $url;
  }
}
?>