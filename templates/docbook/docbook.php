<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once dirname(__FILE__).'/funcs.php';

/**
 * Handles the content of the book, editing, approving, navigation, etc.
 *
 * @package docbook
 */
class docbook extends WebObject
{
  function init()
  {
    $this->addSVar('book_id', UNDEFINED);
    $this->addSVar('chunk', UNDEFINED);
    $this->addSVar('languages', 'en,sq_AL');
    $this->addSVar('lng', 'en');
    $this->addSVar('mode', 'view');  //view | edit | approve | admin
  }

  function on_goto_chunk($event_args)
  {
    $chunk = $event_args['chunk'];
    if ($chunk=='') $chunk = $this->getSVar('book_id');
    $this->setSVar('chunk', $chunk);
  }

  function on_set_chunk_id($event_args)
  {
    $id = $event_args['chunk_id'];
    $path = process_index('edit/get_node_path.xsl', array('id'=>$id));
    $this->setSVar('node_path', $path);
  }

  function on_set_mode($event_args)
  {
    $mode = $event_args['mode'];
    $access_vars = $this->get_button_vars();
    $access_vars['view'] = 'true';
    if ($access_vars[$mode] == 'true')
      {
        $this->setSVar('mode', $mode);
      }
    else
      {
        WebApp::message(T_("Don't have access."));
      }
  }

  function on_set_lng($event_args)
  {
    $lng = $event_args['lng'];
    $this->setSVar('lng', $lng);

    //set book_title
    $book_id = $this->getSVar('book_id');
    $book_title = main::get_book_title($book_id, $lng);
    WebApp::setSVar('book_title', $book_title);
  }

  function onParse()
  {
    $mode = $this->getSVar('mode');
    WebApp::addVar('content', "$mode/$mode.html");
 
    //add variables about access rights of the user
    $this->add_accessright_vars();
  }

  /** add variables about access rights of the user */
  function add_accessright_vars()
  {
    WebApp::addGlobalVar('is_admin', $this->is_admin());
    list($can_edit, $can_approve) = $this->get_edit_rights();
    WebApp::addGlobalVar('can_edit', $can_edit);
    WebApp::addGlobalVar('can_approve', $can_approve);
  }

  /**
   * Returns 'true' if the current user is an admin for the current book,
   * otherwise returns 'false'.
   */
  function is_admin()
  {
    //superuser is considered to be admin of each book
    if (SU=='true')  return 'true';

    //get the books for which the user is admin
    $record = shell(SCRIPTS.'users/get_user.sh '.USER);
    $record = trim($record);
    $arr_fields = explode(':', $record);
    $book_list = $arr_fields[4];
    $arr_books = explode(',', $book_list);

    $book_id = $this->getSVar('book_id');
    $is_admin = (in_array($book_id, $arr_books));
    return ($is_admin ? 'true' : 'false');
  }

  function onRender()
  {
    //add navigation variables
    $chunk = $this->getSVar('chunk');
    $vars = get_arr_navigation($chunk);
    WebApp::addVars($vars);
    WebApp::addVar('toc_path', $this->getSVar('book_id'));

    //add the variables {{edit}}, {{approve}} and {{admin}} which 
    //are used to display the buttons Edit, Approve and Admin
    WebApp::addVars($this->get_button_vars());

    //add state vars
    $arr_state = get_chunk_state();
    $locked = locked_by_somebody($arr_state);
    $str_locked = ($locked ? 'locked' : 'unlocked');
    WebApp::addVar('locked', $str_locked);
    WebApp::addVar('status', $arr_state['status']);
  }

  /**
   * Returns an associative array with keys 'edit', 'approve' and 'admin' 
   * and with values 'true' or 'false', according to the access rights 
   * of the current user.
   */
  function get_button_vars()
  {
    $is_admin = WebApp::getVar('is_admin');
    if ($is_admin==UNDEFINED)
      {
        $this->add_accessright_vars();
        $is_admin = WebApp::getVar('is_admin');
      }

    if ( !defined('EDIT') )
      {
        //no buttons, if not in edit interface
        $edit = 'false';
        $approve = 'false';
        $admin = 'false';
      }
    else if ($is_admin=='true')
      {
        //both buttons, no restrictions
        //for the admins of the book
        $edit = 'true';
        $approve = 'true';

        //the admin button is used only in the root node
        $chunk = WebApp::getSVar('docbook->chunk');
        $book_id = WebApp::getSVar('docbook->book_id');
        $admin = ($chunk==$book_id ? 'true' : 'false');
      }
    else
      {
        //otherwise show the buttons 
        //according to the edit rights of the user
        $edit = WebApp::getVar('can_edit');
        $approve = WebApp::getVar('can_approve');

        //don't display the admin button for no admins
        $admin = 'false';
      }

    $vars = array('edit'=>$edit, 'approve'=>$approve, 'admin'=>$admin);
    return $vars;
  }

  /**
   * Reads the user edit rights file 'admin/access_rights/username'
   * which contains lines like this:
   *     allow:edit,approve:chunk_path_1,chunk_path_2:lng1,lng2
   *     deny:approve:chunk_path_3,chunk_path_4:lng2
   * After matching the current chunk and language with these lines,
   * returns array($edit, $approve) with values 'true' or 'false'.
   */
  function get_edit_rights()
  {
    //initiate them to false
    $edit = 'false';
    $approve = 'false';

    if (!defined('EDIT'))  return array($edit, $approve);

    //get the lines of the edit rights file of the user
    $book_id = $this->getSVar('book_id');
    $accr_file = ADMIN."access_rights/$book_id/".USER;
    $arr_lines = (file_exists($accr_file) ? file($accr_file) : array());

    //try to match the lines with the current node and language
    //and set the permissions accordingly
    for ($i=0; $i < sizeof($arr_lines); $i++)
      {
        $line = $arr_lines[$i];
        $line = trim($line);
        list($access,$levels,$chunks,$langs) = explode(':', $line);

        //if this line matches the current chunk and language 
        //then set the values of edit and approve according to it
        if ($this->chunk_match($chunks) and $this->lang_match($langs))
          {
            $value = ($access=='allow' ? 'true' : 'false');
            $arr_levels = explode(',', $levels);
            if (in_array('edit', $arr_levels))      $edit = $value;
            if (in_array('approve', $arr_levels))   $approve = $value;
          }
      }

    return array($edit, $approve);
  }

  /**
   * Returns true if one of the chunk_path expressions in the list
   * matches the current chunk path, otherwise returns false.
   * The chunk_list is a comma separated list of chunk_path expressions
   * (which are actually regular expressions), or is the string 'ALL'.
   * 'ALL' matches any chunk path.
   */
  function chunk_match($chunk_list)
  {
    if (strtoupper($chunk_list)=='ALL')  return true;

    $chunk = $this->getSVar('chunk');
    $arr_chunks = explode(',', $chunk_list);
    for ($i=0; $i < sizeof($arr_chunks); $i++)
      {
        $expr = $arr_chunks[$i];
        if (ereg('^'.$expr, $chunk)) return true;
      }
  }

  /**
   * Returns true if one of the language ids in the list matches
   * the current language, otherwise returns false. The lang_list
   * is a comma separated list of languages, or 'ALL'.
   * 'ALL' matches any language.
   */
  function lang_match($lang_list)
  {
    if (strtoupper($lang_list)=='ALL')  return true;

    $lng = $this->getSVar('lng');
    $arr_langs = explode(',', $lang_list);
    $match = in_array($lng, $arr_langs);

    return $match;
  }
}
?>