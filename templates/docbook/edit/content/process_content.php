<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * This file contains functions about processing the content of a node
   * that are used by the edit_content webbox and by other modules.
   *
   * @package docbook
   * @subpackage edit
   */

include_once dirname(__FILE__).'/convert_from_xml.php';

/**
 * Builds the content of the node by processing content.xml
 * with xml2mode.xsl, and assigns it to a template variable.
 */
function get_node_content($xml_file, $mode)
{
  //get the content of the xml file
  $xml_content = implode('', file($xml_file));

  //strip <![CDATA[...]]> and <!--comment-->
  $xml_content = strip_comments($xml_content);
  $xml_content = strip_cdata($xml_content);

  //write the modified xml content to a temporary file
  $tmpfile = write_tmp_file($xml_content);

  //generate the html content by processing the temporary file
  if ($mode=='html' or $mode=='text')
    {
      $converter = "xml_to_$mode";
      $html_content = $converter($tmpfile);
    }
  else 
    {
      $xsl_file = XSLT."edit_content/xml2${mode}.xsl";
      $html_content = shell("xsltproc $xsl_file $tmpfile");
    } 
  unlink($tmpfile);

  //put back the <![CDATA[...]]> and <!--comment--> parts
  $html_content = putback_cdata($html_content, 'html');
  if ($mode=='text')  $html_content = compact_cdata($html_content);
  $html_content = putback_comments($html_content);

  //comment any {{variables}} so that they are not substituted
  $html_content = str_replace('{{', '{{#', $html_content);

  return $html_content;
}

/** replace <![CDATA[...]]> by <cdata>x</cdata> */
function strip_cdata($str)
{
  global $arr_cdata;

  preg_match_all('#<!\[CDATA\[.*?]]>#s', $str, $matches);
  $arr_cdata = $matches[0];

  for ($i=0; $i < sizeof($arr_cdata); $i++)
    {
      $str = str_replace($arr_cdata[$i], "<cdata>$i</cdata>", $str);
      $arr_cdata[$i] = str_replace("\r\n", "\n", $arr_cdata[$i]);
    }
      
  return $str;
}

/** replace <!--...--> by <comment>x</comment> */
function strip_comments($str)
{
  global $arr_comments;

  preg_match_all('#<!--.*?-->#s', $str, $matches);
  $arr_comments = $matches[0];

  for ($i=0; $i < sizeof($arr_comments); $i++)
    {
      $str = str_replace($arr_comments[$i], "<comment>$i</comment>", $str);
      $arr_comments[$i] = str_replace("\r\n", "\n", $arr_comments[$i]);
    }
      
  return $str;
}

/** replace <comment>x</comment> by the corresponding <!--...--> */
function putback_comments($str)
{
  global $arr_comments;
  for ($i=0; $i < sizeof($arr_comments); $i++)
    {
      $str = str_replace("<comment>$i</comment>", $arr_comments[$i], $str);
    }
  return $str;
}

/** replace <cdata>x</cdata> by the corresponding <![CDATA[...]]> */
function putback_cdata($str, $mode ='xml')
{
  global $arr_cdata;
  for ($i=0; $i < sizeof($arr_cdata); $i++)
    {
      $cdata = $arr_cdata[$i];
      if ($mode=='html')
        {
          $cdata = preg_replace('#&(\w+);#', '&amp;$1;', $cdata);
          $cdata = preg_replace('#^<#', '&lt;', $cdata);
        }
      $str = str_replace("<cdata>$i</cdata>", $cdata, $str);
    }
  return $str;
}

/**
 * Replace implicit CDATA like this:
 * --tag--
 * abc
 * xyz
 * ----
 * by an explicit CDATA like this:
 * --tag
 * <![CDATA[abc
 * xyz]]>
 * ----
 *
 * Also, replace [[xyz]] by <![CDATA[xyz]]>
 */
function expand_cdata($content)
{
  $content = preg_replace('#\r\n#', "\n", $content);

  $patterns = 
    array(
          // --tag-- ==> --tag <![CDATA[
          '#--(code|scr|screen|ll)-- *\n(.*?)\n---- *\n?#s',

          // [[xyz]] ==> <![CDATA[xyz]]>
          "#(?<!\\\\)\\[\\[(.*?)\\]\\]#s",
          );
  $replacements = 
    array(
          //  --tag-- ==> --tag <![CDATA[
          "--\\1\n<![CDATA[\\2]]>\n----\n",

          // [[xyz]] ==> <![CDATA[xyz]]>
          '<![CDATA[\\1]]>',
          );
  $content = preg_replace($patterns, $replacements, $content);

  return $content;
}

/**
 * Replace explicit CDATA like this:
 * --tag
 * <![CDATA[abc
 * xyz]]>
 * ----  
 * by a compact (implicit) CDATA like this:
 * --tag--
 * abc
 * xyz
 * ----
 *
 * Also, replace <![CDATA[xyz]]> by [[xyz]]
 */
function compact_cdata($content)
{
  // <![CDATA[xyz]]> ==> [[xyz]] 
  $pattern1 = '#&lt;!\[CDATA\[(.*?)\]\]>#s';
  $replacement1 = '[[\\1]]';
  $content = preg_replace($pattern1, $replacement1, $content);

  // --tag-- ==> --tag [[ ==> --tag--
  $pattern2 = '#--(code|scr|screen|ll)\s+\[\[(.*?)\]\]\s*\n---- *\n#s';
  $replacement2 = "--\\1--\n\\2\n----\n";
  $content = preg_replace($pattern2, $replacement2, $content);

  return $content;
}
?>