<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * This file has some common functions.
   *
   * @package docbook
   */

  /*--------------- xml and html files -----------------------*/


  /** Returns the filename of the xml chunk. */
function xml_chunk_file($xml_path =WS_BOOKS, $chunk =UNDEFINED, $lng =UNDEFINED)
{
  if ($chunk==UNDEFINED)
    {
      $chunk = WebApp::getSVar('docbook->chunk');
    }
  if ($lng==UNDEFINED)
    {
      $lng = WebApp::getSVar('docbook->lng');
    }

  $book_id = WebApp::getSVar('docbook->book_id');
  $book_path = $xml_path.$book_id.'/'.$lng.'/';
  $xml_file = $book_path.$chunk.'.xml';

  return $xml_file;
}

/** Returns the filename of the xhtml chunk. */
function xhtml_chunk_file($xhtml_path =WS_XHTML, $chunk =UNDEFINED, $lng =UNDEFINED)
{
  if ($chunk==UNDEFINED)
    {
      $chunk = WebApp::getSVar('docbook->chunk');
    }
  if ($lng==UNDEFINED)
    {
      $lng = WebApp::getSVar('docbook->lng');
    }
  $book_id = WebApp::getSVar('docbook->book_id');
  $book_path = $xhtml_path.$book_id.'/'.$lng.'/';
  $xhtml_file = $book_path.$chunk.'.xhtml';

  return $xhtml_file;
}

/**
 * Reads the file '*.nav.txt' for the given chunk and 
 * returns an associative array with the variables:
 * this_path, next_path, prev_path, up_path, 
 * this_title, next_title, prev_title, up_title.
 */
function get_arr_navigation($chunk, $xhtml_path =UNDEFINED, $lng =UNDEFINED)
{
  if ($xhtml_path==UNDEFINED)
    {
      $xhtml_path = (defined('EDIT') ? WS_XHTML : XHTML);
    }
  if ($lng==UNDEFINED)
    {
      $lng = WebApp::getSVar('docbook->lng');
    }

  $arr_navigation = array();

  //get the navigation file
  $book_id = WebApp::getSVar('docbook->book_id');
  $book_path = $xhtml_path.$book_id.'/'.$lng.'/';
  $fname = $book_path.$chunk.'.nav.txt';
  if (!file_exists($fname))
    {
      $arr_navigation['next_path'] = $book_id;
      $arr_navigation['prev_path'] = $book_id;
      $arr_navigation['up_path']   = $book_id;
      return $arr_navigation;
    }

  //open it
  $lines = file($fname);

  //parse it
  for ($i=0; $i < sizeof($lines); $i++)
    {
      $line = $lines[$i];
      list($var_name,$var_value) = split('=', $line, 2);
      if ($var_name=='')  continue;
      $arr_navigation[$var_name] = trim($var_value);
    }

  //print '<xmp>';  print_r($arr_navigation);  print '</xmp>';    //debug
  return $arr_navigation;
}

/**
 * Update the xhtml file from the xml file. It is done by calling
 * the script:
 *   content/xhtml/xhtml-onechunk.sh book_id chunk lng workspace
 */
function update_xhtml($workspace ='workspace', $chunk =UNDEFINED, $lng =UNDEFINED)
{
  $book_id = WebApp::getSVar('docbook->book_id');

  if ($chunk==UNDEFINED)  $chunk = WebApp::getSVar('docbook->chunk');
  if ($lng==UNDEFINED)    $lng   = WebApp::getSVar('docbook->lng');

  $xhtml_onechunk_sh = CONTENT.'xhtml/xhtml-onechunk.sh';
  $cmd = "$xhtml_onechunk_sh $book_id $chunk $lng $workspace";
  $output = shell($cmd);
  //print "<xmp> funcs.php::update_xhtml() \n $cmd \n $output </xmp>";  //debug
}

/*--------------- state file ----------------------------*/
  
/** Returns the file state.txt of the current chunk. */
function get_state_filename()
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $chunk = WebApp::getSVar('docbook->chunk');
  $lng = WebApp::getSVar('docbook->lng');
  $fname = WS_BOOKS.$book_id.'/'.$lng.'/'.$chunk.'.state.txt';
  return $fname;
}

/**
 * Reads the state file of the current chunk and returns
 * an associative array with the variables found in it. This file
 * contains two lines. The first line is the lock line and contains
 * 'lock:mode:l_user:l_email:l_timestamp', where lock is 'locked' 
 * or 'unlocked' and mode is 'edit' or 'approve'.  The second line
 * is the modification line and it contains 
 * 'status:m_user:m_email:m_timestamp', where status is 'synchronized',
 * or 'modified'.
 * The keys of the associative array are: lock, mode, l_user, l_email,
 * l_timestamp, status, m_user, m_email, m_timestamp.
 */
function get_chunk_state()
{
  $fname = get_state_filename();
  if (!file_exists($fname))
    {
      list($lock, $mode, $l_user, $l_email, $l_timestamp)
        = array('unlocked', '', '', '', '');
      list($status,$m_user,$m_email,$m_timestamp)
        = array('none', '', '', '');
    }
  else
    {
      $lines = file($fname);
      list($lock, $mode, $l_user, $l_email, $l_timestamp)
        = explode(':', chop($lines[0]));
      list($status,$m_user,$m_email,$m_timestamp) 
        = explode(':', chop($lines[1]));
    }
  
  $arr_state = 
    compact('lock', 'mode', 'l_user', 'l_email', 'l_timestamp',
            'status', 'm_user', 'm_email', 'm_timestamp');
  return $arr_state;
}
  
function write_chunk_state($arr_state)
{
  extract($arr_state);
  $str_state = ( "$lock:$mode:$l_user:$l_email:$l_timestamp\n"
                 ."$status:$m_user:$m_email:$m_timestamp\n");
  $fname = get_state_filename();
  write_file($fname, $str_state);
}

/**
 * Set the status of the current chunk to the given new status.
 * The new status can be: synchronized or modified.
 */
function set_chunk_status($new_status)
{
  $arr_state = get_chunk_state();
  $arr_state['status'] = $new_status;
  $arr_state['m_user'] = USER;
  $arr_state['m_email'] = EMAIL;
  $arr_state['m_timestamp'] = time();
  write_chunk_state($arr_state);
}

/**
 * Lock or unlock the current chunk.
 * The $lock parameter can be 'locked' or 'unlocked',
 * and $mode can be 'edit' or 'approve'.
 */
function set_chunk_lock($lock, $mode)
{
  //make sure that the chunk is not locked by somebody else
  $arr_state = get_chunk_state();
  if (locked_by_somebody($arr_state))  return;

  $arr_state['lock'] = $lock;
  if ($lock=='locked')
    {
      $arr_state['mode'] = $mode;
      $arr_state['l_user'] = USER;
      $arr_state['l_email'] = EMAIL;
      $arr_state['l_timestamp'] = time();
    }
  write_chunk_state($arr_state);
}

/**
 * Returns true and displays a notification message,
 * if the chunk is locked by another user.
 */
function locked_by_somebody($arr_state =UNDEFINED)
{
  if ($arr_state==UNDEFINED)  $arr_state = get_chunk_state();
  if ($arr_state['l_user']==USER) return false;
  return is_locked($arr_state);
}

/** Returns true if the chunk is locked and the lock has not expired. */
function is_locked($arr_state)
{
  extract($arr_state);
  if ($lock=='unlocked')  return false;

  //check whether the lock has expired (more than 15 minutes ago
  //from the lock time and from the last modification)
  if ( (time() - $l_timestamp) > 15*60 )
    {
      if ($m_timestamp=='')  return false;
      if ( (time() - $m_timestamp) > 15*60 )  return false;
    }

  return true;
}

/**
 * Converts the given time stamp to a date string.
 * If the time is today, then give just hours and minutes,
 * otherwise give more information.
 */
function get_date_str($timestamp)
{
  $day = 24*60*60;  //nr of secs in a day
  $t = getdate(time());
  $midnight = mktime(1, 1, 0, $t['mon'], $t['mday'], $t['year']);
  $firstday = mktime(1, 1, 0, 1, 1, $t['year']);

  if ($timestamp > $midnight)
    {
      $date = date('H:i', $timestamp);
      $str = T_("today at v_date_H:i");
      $str = str_replace('v_date_H:i', $date, $str);
      return $str;
    }

  if ($timestamp > $midnight - $day)
    {
      $date = date('H:i', $timestamp);
      $str = T_("yesterday at v_date_H:i");
      $str = str_replace('v_date_H:i', $date, $str);
      return $str;
    }

  if ($timestamp > $midnight - 6*$day)
    {
      $date = date('D H:i', $timestamp);
      $str = T_("on v_date_D_H:i");
      $str = str_replace('v_date_D_H:i', $date, $str);
      return $str;
    }

  if ($timestamp > $firstday)
    {
      $date = date('M d, H:i', $timestamp);
      $str = T_("on v_date_M_d_H:i");
      $str = str_replace('v_date_M_d_H:i', $date, $str);
      return $str;
    }

  //older than the first day of this year
  $date = date('M d, Y', (int)$timestamp);
  $str = T_("on v_date_M_d_Y");
  $str = str_replace('v_date_M_d_Y', $date, $str);
  return $str;
}


/**
 * Check whether the public copy of the book is fixed to
 * a certain tag or revision and return it. If book is not
 * fixed, return false.
 */
function book_fixed_to_tag($book_id, $lng)
{
  $book_dir = BOOKS."$book_id/$lng/";

  if (file_exists($book_dir.'fixed'))
    {
      $lines = file($book_dir.'fixed');
      $tag = trim($lines[0]);
      return $tag;
    }
  else
    return false;
}
?>