<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once TPL.'languages/func.languages.php';

/**
 * A list of languages in which the book is available.
 *
 * @package docbook
 */
class langs extends WebObject
{
  function onRender()
  {
    $langs = WebApp::getSVar('docbook->languages');
    $arr_langs = explode(',', $langs);
    $arr_lng_details = get_arr_languages();

    $rs = new EditableRS("langs");
    for ($i=0; $i < sizeof($arr_langs); $i++)
      {
        $id = $arr_langs[$i];
        $label = $arr_lng_details[$id]['name'];
        $rs->addRec(compact('id', 'label'));
      }
     
    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>