<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Display the xhtml file for the current chunk of the book.
   *
   * @package docbook
   */
class view extends WebObject
{
  function init()
  {
    WebApp::setSVar('webnotes->visible', 'false');
  }

  function onParse()
  {
    $book_id = WebApp::getSVar('docbook->book_id');
    $chunk = WebApp::getSVar('docbook->chunk');
    $lng = WebApp::getSVar('docbook->lng');

    $xhtml = (defined('EDIT') ? WS_XHTML : XHTML);
    $book_path = $xhtml.$book_id.'/'.$lng.'/';

    $content_file = $book_path.$chunk.'.xhtml';

    WebApp::addVar('content_file', $content_file);

    $this->setup_webnotes();
  }

  /** set propper values to the parameters of the 'webnotes' component */
  function setup_webnotes()
  {
    if (WEBNOTES_ENABLE=='false')
      {
        WebApp::setSVar('webnotes->page_id', '');
        return;
      }

    //get the book settings
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $chunk = WebApp::getSVar('docbook->chunk');

    //generate a page_id for the current node
    ereg('([^/]+)/$', $chunk, $regs);
    $node_id = $regs[1];
    $page_id = "$book_id/$lng/$node_id";

    if (WEBNOTES_NOTIFY=='true')
      {
        //notify admins of the book about the new notes;
        $notify = 'true';
        //get a list of emails of the admins of the book
        $users = ADMIN.'access_rights/users';
        $cmd = "gawk -F: '(\$5 ~ \"$book_id\") {print \$4}' $users";
        $output = shell($cmd);
        $arr_emails = explode("\n", trim($output));
        $admin_emails = implode(',', $arr_emails);
      }
    else
      {
        $notify = 'false';
        $admin_emails = '';
      }

    //notes are approved immediatly
    $unmoderated = (WEBNOTES_APPROVE=='true' ? 'false' : 'true');

    //a user can moderate the notes of the node
    //if he has approve rights on the node
    $approve = WebApp::getVar('can_approve');

    //a user can admin webnotes if he is admin of the book
    $admin = WebApp::getVar('is_admin'); 

    //he can admin the notes for all the nodes of the book 
    $admin_pageid_filter = "page_id LIKE '$book_id/%'";

    //set the parameters of the webnotes
    WebApp::setSVar('webnotes->page_id', $page_id);
    WebApp::setSVar('webnotes->notify', $notify);
    WebApp::setSVar('webnotes->emails', $admin_emails);
    WebApp::setSVar('webnotes->unmoderated', $unmoderated);
    WebApp::setSVar('webnotes->approve', $approve);
    WebApp::setSVar('webnotes->admin', $admin);
    WebApp::setSVar('webnotes->admin_pageid_filter', $admin_pageid_filter);

    //set the database parameters
    WebApp::setSVar('webnotes->dbhost',   WEBNOTES_DBHOST);
    WebApp::setSVar('webnotes->dbuser',   WEBNOTES_DBUSER);
    WebApp::setSVar('webnotes->dbpasswd', WEBNOTES_DBPASS);
    WebApp::setSVar('webnotes->dbname',   WEBNOTES_DBNAME);
  }
}
?>