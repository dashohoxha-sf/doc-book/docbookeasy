<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * The stating point for constructing the web page.
   */
class main extends WebObject
{
  function init()
  {
    //get the book that will be displayed initially
    $book_id = $this->check_book_id(DEFAULT_BOOK);

    //get languages
    $arr_langs = $this->get_langs($book_id);
    $langs = implode(',', $arr_langs);
    $lng = (in_array(LNG, $arr_langs) ? LNG : $arr_langs[0]);
 
    //set book_title
    $book_title = $this->get_book_title($book_id, $lng);
    WebApp::addSVar('book_title', $book_title);
 
    //init docbook state variables
    WebApp::addSVar('docbook->book_id', $book_id);
    WebApp::addSVar('docbook->chunk', $book_id);
    WebApp::addSVar('docbook->languages', $langs);
    WebApp::addSVar('docbook->lng', $lng);
    WebApp::addSVar('docbook->mode', 'view');

    //display docbook in the content part
    $this->addSVar('content_file', 'docbook/docbook.html'); 
  }

  /**
   * Checks whether the given book_id exists or not.
   * If it does not exist, display an error message
   * and return a book_id that does exist.
   */
  function check_book_id($book_id)
  {
    if (!file_exists(BOOKS.$book_id))
      {
        $msg = T_("Book 'v_book_id' not found.");
        $msg = str_replace('v_book_id', $book_id, $msg);
        WebApp::message($msg);
        $book_id = 'docbookeasy';
      }
    return $book_id;
  }

  function on_book($event_args)
  {
    $book_id = $event_args['book_id'];
    $book_id = $this->check_book_id($book_id);

    //get languages
    $arr_langs = $this->get_langs($book_id);
    $langs = implode(',', $arr_langs);

    //check lng (in case that the current language 
    //is not available for this book)
    $lng = WebApp::getSVar('docbook->lng');
    if (!in_array($lng, $arr_langs))
      {
        $lng = $arr_langs[0];
        WebApp::setSVar('docbook->lng', $lng);
      }

    //set book_title
    $book_title = $this->get_book_title($book_id, $lng);
    WebApp::setSVar('book_title', $book_title);

    //set some other state variables
    WebApp::setSVar('docbook->book_id', $book_id);
    WebApp::setSVar('docbook->languages', $langs);
    WebApp::setSVar('docbook->chunk', $book_id);
  }

  function on_approve($event_args)
  {
    extract($event_args);
    if (!isset($book_id))  $book_id = WebApp::getSVar('docbook->book_id');
    if (!isset($chunk)) $chunk = WebApp::getSVar('docbook->chunk');
    if (!isset($lng))  $lng = WebApp::getSVar('docbook->lng');

    //get languages
    $arr_langs = $this->get_langs($book_id);
    $langs = implode(',', $arr_langs);

    WebApp::setSVar('docbook->book_id', $book_id);
    WebApp::setSVar('docbook->chunk', $chunk);
    WebApp::setSVar('docbook->lng', $lng);
    WebApp::setSVar('docbook->languages', $langs);
    WebApp::setSVar('docbook->mode', 'approve');

    //set book_title
    $book_title = $this->get_book_title($book_id, $lng);
    WebApp::setSVar('book_title', $book_title);
  }

  function on_search($event_args)
  {
    $words = $event_args['words'];
    if (trim($words) != '')
      {
        WebApp::setSVar('search->expression', trim($words));
        WebApp::setSVar('search->all_books', '');
        WebApp::setSVar('search->all_langs', '');
        WebApp::setSVar('search->recs_per_page', '20');
        WebApp::setSVar('search->current_page', '1');
      }
    else { /* do nothing, display the last search */ }

    $this->setSVar('content_file', 'main/search/search.html');
  }

  function on_docbook($event_args)
  {
    $this->setSVar('content_file', 'docbook/docbook.html');
    if (!isset($event_args['node_path']))  return;

    $book_id = $event_args['book_id'];

    //get languages
    $arr_langs = $this->get_langs($book_id);
    $langs = implode(',', $arr_langs);

    if (!file_exists(BOOKS.$book_id))
      {
        $msg = T_("Book 'v_book_id' not found.");
        $msg = str_replace('v_book_id', $book_id, $msg);
        WebApp::message($msg);
        $book_id = 'docbookeasy';
        $lng = 'en';
        $chunk = $book_id;
      }
    else
      {
        $chunk = $event_args['chunk'];
        $lng = $event_args['lng'];
        if ($lng=='')
          {
            $msg = T_("Language 'v_lng' not found.");
            $msg = str_replace('v_lng', $lng, $msg);
            WebApp::message($msg);
            $lng = $arr_langs[0];
          }
      }

    //set docbook state variables
    WebApp::setSVar('docbook->book_id', $book_id);
    WebApp::setSVar('docbook->chunk', $chunk);
    WebApp::setSVar('docbook->languages', $langs);
    WebApp::setSVar('docbook->lng', $lng);

    //set book_title
    $book_title = $this->get_book_title($book_id, $lng);
    WebApp::setSVar('book_title', $book_title);
  }

  /** this event is called by xref.php */
  function on_xref($event_args)
  {
    $this->setSVar('content_file', 'docbook/docbook.html');

    $book_id = $event_args['book_id'];

    //get languages
    $arr_langs = $this->get_langs($book_id);
    $langs = implode(',', $arr_langs);

    if (!file_exists(BOOKS.$book_id))
      {
        $msg = T_("Book 'v_book_id' not found.");
        $msg = str_replace('v_book_id', $book_id, $msg);
        WebApp::message($msg);
        $book_id = 'docbookeasy';
        $lng = 'en';
        $chunk = $book_id;
      }
    else
      {
        $lng = $event_args['lng'];
        if ($lng=='') $lng = $arr_langs[0];

        //get node path from node id
        $node_id = $event_args['node_id'];
        $xsl = XSLT.'edit/get_node_path.xsl';
        $index_xml = BOOKS."$book_id/$lng/index.xml";
        $param_id = "--stringparam id '$node_id'";
        $chunk = shell("xsltproc $param_id $xsl $index_xml");
        if ($chunk=='')  $chunk = $book_id;
      }
    $mode = $event_args['mode'];
    if ($mode=='')  $mode = 'view';

    //set the state variables of the docbook webbox
    WebApp::setSVar('docbook->book_id', $book_id);
    WebApp::setSVar('docbook->chunk', $chunk);
    WebApp::setSVar('docbook->languages', $langs);
    WebApp::setSVar('docbook->lng', $lng);
    WebApp::setSVar('docbook->mode', $mode);

    //set book_title
    $book_title = $this->get_book_title($book_id, $lng);
    WebApp::setSVar('book_title', $book_title);
  }


  function onRender()
  {
    WebApp::addVar("search_button", T_("Search >>"));
  }

  /**
   * Returns an array of languages (en,fr,it,al) 
   * in which the book is available.
   */
  function get_langs($book_id)
  {
    $book_path = BOOKS.$book_id.'/';
    if (!file_exists($book_path))  return array();

    $langs = shell("ls $book_path");
    $arr_langs = explode("\n", chop($langs));

    return $arr_langs;
  }

  function get_book_title($book_id, $lng)
  {
    $book_list = CONTENT.'books/book_list';
    $line = shell("sed -n '/^$book_id:$lng:/p' $book_list");
    $line = ereg_replace("\n.*", '', $line);
    $arr = split(':', $line, 3);
    $title = trim($arr[2]);

    return $title;
  }
}
?>