#!/bin/bash
### called by approve.php to commit the node of a book

### get parameters
node_dir=$1
msg=$2

### commit the directory non-recursively
svn status -N $node_dir \
  | grep -v '^\? ' \
  | sed -e 's/^.\{7\}//' \
  | xargs svn commit -m "$msg"
