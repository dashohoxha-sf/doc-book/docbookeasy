#!/bin/bash
### create the access file for the given user in the given book

### get the parameters book_id and username
book_id=$1
username=$2

### create the access rights directory for the book
book_access_rights=templates/admin/access_rights/$book_id
mkdir -p $book_access_rights

### create the access rights file for the user
touch $book_access_rights/$username
