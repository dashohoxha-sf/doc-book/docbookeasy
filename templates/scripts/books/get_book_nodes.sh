#!/bin/bash
### get from the given book index the list of the node_id's

### get the path of the file index.xml
index_xml=$1

### get the path of the xsl file
path=$(dirname $0)
xsl_file=$path/get_book_nodes.xsl

### process index.xml with the xsl stylesheet
xsltproc $xsl_file $index_xml
