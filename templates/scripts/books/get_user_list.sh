#!/bin/bash
### get the list of the users of the book, in the given range

### get the parameters
book_id=$1
first=$2
last=$3

### get the list of users, sort it and extract only a part of it
book_access_rights=templates/admin/access_rights/$book_id
if [ -d $book_access_rights ]
then
  ls $book_access_rights | sort | sed -n "$first,${last}p"
fi