#!/bin/bash
### get the number of the users of the given book

### get the parameter book_id
book_id="$1"

### get a list of users and count them
book_access_rights=templates/admin/access_rights/$book_id
ls $book_access_rights | wc -l
