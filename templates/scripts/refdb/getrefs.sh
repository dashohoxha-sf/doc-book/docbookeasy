#!/bin/bash
### Get referencies from the RefDB server in XML/DocBook format.
### The search line is in the file given as parameter.

### get the file that contains the query (search line)
query_file=$1

### get refdb connection settings from 'docbookeasy.conf'
### it also contains the path of the refdbc command
. docbookeasy.conf
host=$REFDB_HOST
user=$REFDB_USER
passwd=$REFDB_PASS
database=$REFDB_DATABASE
encoding=$REFDB_ENCODING

### get the referencies
$REFDBC -i $host -u $user -w "$passwd" -d $database \
        -C getref -t db31x -s "ALL" -e $encoding -f $query_file \
        2> /dev/null \
  | sed -e '1,3d' -e '$d'
