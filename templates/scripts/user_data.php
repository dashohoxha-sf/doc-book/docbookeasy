<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package admin
   */

function get_user_data($username)
{
  $user_record = shell(SCRIPTS."users/get_user.sh $username");
  if (trim($user_record)=='')  $user_record = "::::";
  $arr = explode(':', chop($user_record));
  $user_data = array(
                     'username' => $arr[0],
                     'password' => $arr[1],
                     'name'     => $arr[2],
                     'email'    => $arr[3],
                     'books'    => $arr[4]
                     );
  return $user_data;
}

function save_user_data($user_data)
{
  $username = $user_data['username'];
  $user_record = implode(':', $user_data);
  shell(SCRIPTS."users/del_user.sh $username");
  shell(SCRIPTS."users/add_user.sh '$user_record'");
}
?>