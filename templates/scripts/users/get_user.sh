#!/bin/bash
### get the record of the given user

### get the username from the first parameter
username=$1

### return the record for the given user
users=templates/admin/access_rights/users
sed -n "/^$username:/p" $users
