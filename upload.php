<?php
  /*
   This file is part of DocBookEasy.  DocBookEasy is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookEasy is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookEasy is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookEasy;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once 'config/const.Paths.php';
include_once 'config/const.Options.php';
include_once 'global.php';

//load the translations of the messages
set_locale();

//get the variable upload_source and load the corresponding module
//which contains the function upload_files(); this function should be
//specific for each upload source
$upload_source = $_POST['upload_source'];
include_once "upload/$upload_source.php";

//call the function upload_files()
upload_files();

//display a message about the upload
include_once 'upload/upload_message.html';

exit;

//--------------- functions ---------------------

/** load the translations of the messages */
function set_locale()
{
  $app_name = basename(dirname(dirname(__FILE__)));
  $domain = $app_name;
  if (LNG != UNDEFINED)  setlocale(LC_MESSAGES, LNG);
  if (CODESET != UNDEFINED)  bind_textdomain_codeset($domain, CODESET);
  bindtextdomain($domain, 'l10n/');
  textdomain($domain);
}

/** translate a message */
function T_($msgid)
{
  return gettext($msgid);
}
/**
 * Check the error status of the given file and return an appropriate
 * message. If the upload is OK, an empty message is returned.
 * It is called by the function upload_files().
 */

function check_upload_status($file)
{
  $error = $_FILES[$file]['error'];
  switch ($error)
    {
    case UPLOAD_ERR_INI_SIZE:
      $msg = T_("File 'v_fname' exceeds the upload_max_filesize directive in php.ini.");
      break;

    case UPLOAD_ERR_FORM_SIZE:
      $msg = T_("File 'v_fname' exceeds the MAX_FILE_SIZE directive that was specified in the html form.");
      break;

    case UPLOAD_ERR_PARTIAL:
      $msg = T_("File 'v_fname' was only partially uploaded. Please try again.");
      break;

    case UPLOAD_ERR_NO_FILE:
      $msg = T_("No file was uploaded. Please try again.");
      break;

    default:
    case UPLOAD_ERR_OK:
      $msg = '';
      break;
    }

  $fname = $_FILES[$file]['name'];
  $fname = basename($fname);
  $msg = str_replace('v_fname', $fname, $msg);

  return $msg;
}

/**
 * Move the temporary file to the destination file.
 * Returns a message about the status of the operation. 
 * It is called by the function upload_files().
 */
function move_file($tmp_file, $dest_file)
{
  //check that the file is uploaded
  if (!is_uploaded_file($tmp_file))
    $msg = T_("File 'v_fname' not uploaded, there is some error.");

  //move the uploaded file to the destination
  chmod($tmp_file, 0644);
  $dest_dir = dirname($dest_file);
  shell("mkdir -p $dest_dir");
  shell("cp $tmp_file $dest_file");
  unlink($tmp_file);

  //check that the file was moved successfully
  if (file_exists($dest_file))
    $msg = T_("File 'v_fname' uploaded successfully.");
  else 
    $msg = T_("File 'v_fname' is not uploaded for some reasons.");

  $msg = str_replace('v_fname', basename($dest_file), $msg);
  return $msg;
}
?>