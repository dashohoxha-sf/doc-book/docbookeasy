<?xml version='1.0'?>
<!--
    This  file   is  part  of  DocBookEasy.   DocBookEasy   is  a  web
    application that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sf.net

    DocBookEasy  is  free software;  you  can  redistribute it  and/or
    modify it  under the  terms of the  GNU General Public  License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    DocBookEasy is distributed in the hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of
    MERCHANTABILITY or FITNESS FOR  A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a  copy of the GNU General Public License
    along  with  DocBookEasy;  if  not,  write to  the  Free  Software
    Foundation,  Inc.,   59  Temple  Place,  Suite   330,  Boston,  MA
    02111-1307 USA
  -->


<!--
    This stylesheet is a customization of the docbook chunking stylesheets.
    Instead of producing XHTML chunks, it creates XML chunks. This is done
    in this way:
    1 - For formating is imported 'docbook-copy.xsl', which does not do
        any formating but instead copies each element recursivly.
    2 - We give to the chunker parameters such values so that the id
        of the chunked element is used as filename, and the extension
        of the chunk files is '.xml'.
    3 - The template 'chunk-element-content' (copied from the file
        'xhtml/chunk-common.xsl') is customized so that it just outputs
        the content and does not produce any headers, footers, etc.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">


<xsl:import href="copy.xsl"/>
<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk-common.xsl"/>
<xsl:include href="http://docbook.sourceforge.net/release/xsl/current/xhtml/manifest.xsl"/>
<xsl:include href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk-code.xsl"/>

<xsl:param name="chunker.output.omit-xml-declaration" select="'no'"/>
<xsl:param name="chunker.output.doctype-public"
           select="'-//OASIS//DTD DocBook XML V4.4//EN'"/>
<xsl:param name="chunker.output.doctype-system"
           select="'http://docbook.org/xml/4.4/docbookx.dtd'"/>

<xsl:param name="chunker.output.encoding" select="'UTF-8'"/>
<xsl:param name="chunker.output.standalone" select="'no'"/>
<xsl:param name="chunker.output.indent" select="'yes'"/>
<!-- <xsl:param name="chunker.output.cdata-section-elements" select="''"/> -->

<xsl:param name="root.filename" select="'main'"/>
<xsl:param name="use.id.as.filename" select="1"/>
<xsl:param name="html.ext" select="'.xml'"/>
<xsl:param name="chunk.first.sections" select="1"></xsl:param>
<xsl:param name="chunk.fast" select="1"/>
<xsl:param name="chunk.quietly" select="1"/>


<!-- customized so that it just outputs the content -->
<!-- and does not produce any headers, footers, etc.  -->
<xsl:include href="../template/chunk-element-content.xsl"/>

<!-- include a customized version of the template "process-chunk" -->
<xsl:include href="../template/process-chunk.xinclude.xsl"/>


</xsl:stylesheet>
