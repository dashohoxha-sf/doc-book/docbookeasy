<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
    This  file   is  part  of  DocBookEasy.   DocBookEasy   is  a  web
    application that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sf.net

    DocBookEasy  is  free software;  you  can  redistribute it  and/or
    modify it  under the  terms of the  GNU General Public  License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    DocBookEasy is distributed in the hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of
    MERCHANTABILITY or FITNESS FOR  A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a  copy of the GNU General Public License
    along  with  DocBookEasy;  if  not,  write to  the  Free  Software
    Foundation,  Inc.,   59  Temple  Place,  Suite   330,  Boston,  MA
    02111-1307 USA
  -->

<!-- 
     Appends a new <xi:include> element at the end of the given xml chunk,
     which has as attribute the given href.
     Is called with a parameter 'href', like this: 
     xsltproc -stringparam href $href add_subchunk.xsl chunk.xml
  -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" encoding="utf-8" 
              omit-xml-declaration="no" standalone="no" indent="yes" />

  <xsl:template match="/*">
    <xsl:copy>
      <!-- copy any attriutes -->
      <xsl:copy-of select="@*" />
      <!-- copy subelements -->
      <xsl:copy-of select="*"/>
      <!-- append a new <xi:include> element for the new subchunk -->
      <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="{$href}" />
    </xsl:copy>
  </xsl:template>

</xsl:transform>

