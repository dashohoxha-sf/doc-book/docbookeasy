<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
    This  file   is  part  of  DocBookEasy.   DocBookEasy   is  a  web
    application that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sf.net

    DocBookEasy  is  free software;  you  can  redistribute it  and/or
    modify it  under the  terms of the  GNU General Public  License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    DocBookEasy is distributed in the hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of
    MERCHANTABILITY or FITNESS FOR  A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a  copy of the GNU General Public License
    along  with  DocBookEasy;  if  not,  write to  the  Free  Software
    Foundation,  Inc.,   59  Temple  Place,  Suite   330,  Boston,  MA
    02111-1307 USA
  -->

<!-- 
     Updates the attribute id of the root element and the attributes
     href of the <xi:include> elements of the given chunk.
     It is called with the parameter 'new_id', like this:
     xsltproc -stringparam new_id $new_id \
     change_chunk_id.xsl chunk.xml
  -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" encoding="utf-8" 
              omit-xml-declaration="no" standalone="no" indent="yes" />


  <!-- replace the attribute id of the root element with the new id -->
  <xsl:template match="/*/@id">
    <xsl:attribute name="id">
      <xsl:value-of select="$new_id"/>
    </xsl:attribute>
  </xsl:template>


  <!-- modify the attribute href of <xi:include> elements -->
  <xsl:template match="xi:include/@href" xmlns:xi="http://www.w3.org/2001/XInclude">
    <xsl:attribute name="href">
      <xsl:value-of select="concat($new_id, '/', substring-after(., '/'))"/>
    </xsl:attribute>
  </xsl:template>


  <!-- copy the rest unmodified -->
  <xsl:template match="*|@*">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="node()" />
    </xsl:copy>
  </xsl:template>


</xsl:transform>

