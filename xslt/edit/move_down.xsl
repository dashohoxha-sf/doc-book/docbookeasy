<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
    This  file   is  part  of  DocBookEasy.   DocBookEasy   is  a  web
    application that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sf.net

    DocBookEasy  is  free software;  you  can  redistribute it  and/or
    modify it  under the  terms of the  GNU General Public  License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    DocBookEasy is distributed in the hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of
    MERCHANTABILITY or FITNESS FOR  A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a  copy of the GNU General Public License
    along  with  DocBookEasy;  if  not,  write to  the  Free  Software
    Foundation,  Inc.,   59  Temple  Place,  Suite   330,  Boston,  MA
    02111-1307 USA
  -->

<!-- 
     Transform the chunk by exchanging the place of the <xi:include>
     element which has the given href, with the following one (if there is
     a following sibling <xi:include> element).
     Is called with a parameter 'href', like this: 
     xsltproc -stringparam href $href move_down.xsl chunk.xml
  -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" version="1.0" encoding="utf-8" 
              omit-xml-declaration="no" standalone="no" indent="yes" />


  <xsl:template match="xi:include" xmlns:xi="http://www.w3.org/2001/XInclude">
    <xsl:choose>

      <!-- if this element is the selected one and there exists a following -->
      <!-- element, then copy the following element instead of this one     -->
      <xsl:when test="@href=$href and following-sibling::xi:include">
	<xsl:copy-of select="following-sibling::xi:include[1]" />
      </xsl:when>

      <!-- if the element previous to this one is the selected one, -->
      <!-- then copy the previous element instead of this one       -->
      <xsl:when test="(preceding-sibling::xi:include)[position()=last()]/@href=$href">
	<xsl:copy-of select="preceding-sibling::xi:include[1]" />
      </xsl:when>

      <!-- otherwise copy this section and apply templates -->
      <xsl:otherwise>
	<xsl:copy-of select="."/>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>


  <!-- copy everything else -->
  <xsl:template match="*|@*">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="node()" />
    </xsl:copy>
  </xsl:template>


</xsl:transform>
