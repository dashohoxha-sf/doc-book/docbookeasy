#!/bin/bash

book_path=../../../content/books/xml/docbookeasy/en
xml_chunk=$book_path/docbookeasy/administrating.xml
href=administrating/book_editors.xml

xsltproc --stringparam href $href ../move_up.xsl $xml_chunk
