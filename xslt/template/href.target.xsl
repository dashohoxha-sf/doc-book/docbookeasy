<?xml version='1.0'?>
<!--
    This  file   is  part  of  DocBookEasy.   DocBookEasy   is  a  web
    application that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sf.net

    DocBookEasy  is  free software;  you  can  redistribute it  and/or
    modify it  under the  terms of the  GNU General Public  License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    DocBookEasy is distributed in the hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of
    MERCHANTABILITY or FITNESS FOR  A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a  copy of the GNU General Public License
    along  with  DocBookEasy;  if  not,  write to  the  Free  Software
    Foundation,  Inc.,   59  Temple  Place,  Suite   330,  Boston,  MA
    02111-1307 USA
  -->

<!--
    The template href.target is modified so that it makes a call to a
    javascript function.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template name="href.target">
  <xsl:param name="context" select="."/>
  <xsl:param name="object" select="."/>
  <xsl:param name="toc-context" select="."/>

  <xsl:variable name="chunkpath">
    <xsl:call-template name="chunk-path">
      <xsl:with-param name="object" select="$object"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="href">
    <xsl:text>javascript:goto_chunk('</xsl:text>
    <xsl:value-of select="$chunkpath"/>
    <xsl:text>')</xsl:text>
  </xsl:variable>

  <xsl:value-of select="$href"/>
</xsl:template>


<xsl:template name="chunk-path">
  <xsl:param name="object" select="."/>

  <xsl:variable name="href.to.uri">
    <xsl:call-template name="href.target.uri">
      <xsl:with-param name="object" select="$object"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:value-of select="substring-before($href.to.uri, $html.ext)"/>
</xsl:template>


</xsl:stylesheet>
