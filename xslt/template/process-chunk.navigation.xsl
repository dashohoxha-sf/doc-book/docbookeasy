<?xml version='1.0'?>
<!--
    This  file   is  part  of  DocBookEasy.   DocBookEasy   is  a  web
    application that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sf.net

    DocBookEasy  is  free software;  you  can  redistribute it  and/or
    modify it  under the  terms of the  GNU General Public  License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    DocBookEasy is distributed in the hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of
    MERCHANTABILITY or FITNESS FOR  A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a  copy of the GNU General Public License
    along  with  DocBookEasy;  if  not,  write to  the  Free  Software
    Foundation,  Inc.,   59  Temple  Place,  Suite   330,  Boston,  MA
    02111-1307 USA
  -->

<!--
    This customization writes also a navigation file (prev, next, etc.)
    for each chunk.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- create also a navigation file -->
<xsl:param name="create.navigation.file" select="'yes'"/>

<xsl:template name="process-chunk">
  <xsl:param name="prev" select="."/>
  <xsl:param name="next" select="."/>
  <xsl:param name="content">
    <xsl:apply-imports/>
  </xsl:param>

  <xsl:variable name="ischunk">
    <xsl:call-template name="chunk"/>
  </xsl:variable>

  <xsl:variable name="chunkfn">
    <xsl:if test="$ischunk='1'">
      <xsl:apply-templates mode="chunk-filename" select="."/>
    </xsl:if>
  </xsl:variable>

  <xsl:if test="$ischunk='0'">
    <xsl:message>
      <xsl:text>Error </xsl:text>
      <xsl:value-of select="name(.)"/>
      <xsl:text> is not a chunk!</xsl:text>
    </xsl:message>
  </xsl:if>

  <xsl:variable name="filename">
    <xsl:call-template name="make-relative-filename">
      <xsl:with-param name="base.dir" select="$base.dir"/>
      <xsl:with-param name="base.name" select="$chunkfn"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="write.chunk">
    <xsl:with-param name="filename" select="$filename"/>
    <xsl:with-param name="content">
      <xsl:call-template name="chunk-element-content">
        <xsl:with-param name="prev" select="$prev"/>
        <xsl:with-param name="next" select="$next"/>
        <xsl:with-param name="content" select="$content"/>
      </xsl:call-template>
    </xsl:with-param>
    <xsl:with-param name="quiet" select="$chunk.quietly"/>
  </xsl:call-template>

  <!-- dasho -->
  <!-- create also a navigation file, if $create.navigation.file is 'yes' -->
  <xsl:if test="$create.navigation.file = 'yes'">
    <xsl:variable name="filename-nav">
      <xsl:value-of select="substring-before($filename, $html.ext)"/>
      <xsl:value-of select="'.nav.txt'"/>
    </xsl:variable>

    <xsl:call-template name="write.chunk">
      <xsl:with-param name="filename" select="$filename-nav"/>
      <xsl:with-param name="content">
	<xsl:call-template name="chunk-element-navigation">
	  <xsl:with-param name="prev" select="$prev"/>
	  <xsl:with-param name="next" select="$next"/>
	</xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="quiet" select="$chunk.quietly"/>
    </xsl:call-template>
  </xsl:if>

</xsl:template>


<!-- creates the content of the navigation file for the current chunk -->
<xsl:template name="chunk-element-navigation">
  <xsl:param name="prev" select="."/>
  <xsl:param name="next" select="."/>

  <xsl:variable name="this-path">
    <xsl:call-template name="chunk-path">
      <xsl:with-param name="object" select="."/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="this-title">
    <xsl:value-of select="./title"/>
  </xsl:variable>

  <xsl:variable name="up-path">
    <xsl:call-template name="chunk-path">
      <xsl:with-param name="object" select=".."/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="up-title">
    <xsl:value-of select="../title"/>
  </xsl:variable>

  <xsl:variable name="prev-path">
    <xsl:call-template name="chunk-path">
      <xsl:with-param name="object" select="$prev"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="prev-title">
    <xsl:value-of select="$prev/title"/>
  </xsl:variable>

  <xsl:variable name="next-path">
    <xsl:call-template name="chunk-path">
      <xsl:with-param name="object" select="$next"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="next-title">
    <xsl:value-of select="$next/title"/>
  </xsl:variable>

  <!-- output navigation variables -->
  <xsl:value-of select="concat('this_path=', $this-path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('this_title=', $this-title)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('up_path=', $up-path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('up_title=', $up-title)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('prev_path=', $prev-path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('prev_title=', $prev-title)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('next_path=', $next-path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('next_title=', $next-title)"/><xsl:text>
</xsl:text>

</xsl:template>


</xsl:stylesheet>
