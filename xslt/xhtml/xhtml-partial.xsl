<?xml version='1.0'?>
<!--
    This  file   is  part  of  DocBookEasy.   DocBookEasy   is  a  web
    application that displays and edits DocBook documents.

    Copyright (C) 2008 Dashamir Hoxha, dashohoxha@users.sf.net

    DocBookEasy  is  free software;  you  can  redistribute it  and/or
    modify it  under the  terms of the  GNU General Public  License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    DocBookEasy is distributed in the hope that it will be useful, but
    WITHOUT  ANY  WARRANTY;  without  even  the  implied  warranty  of
    MERCHANTABILITY or FITNESS FOR  A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a  copy of the GNU General Public License
    along  with  DocBookEasy;  if  not,  write to  the  Free  Software
    Foundation,  Inc.,   59  Temple  Place,  Suite   330,  Boston,  MA
    02111-1307 USA
  -->

<!--
    This stylesheet generates XHTML chunks, but only for the ancestors
    and siblings of the selected chunk. It is an attempt to make the XHTML
    generation a bit more efficient, by regenerating only those parts that
    need to be regenerated.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">


<xsl:import href="xhtml-plain.xsl"/>


<!-- include a customized version of the template "process-chunk" -->
<xsl:include href="../template/chunk-filename.xsl"/>

<!-- don't create a navigation file -->
<xsl:param name="create.navigation.file" select="'yes'"/>

<!-- the id of the chunk to be processed -->
<xsl:param name="chunk.id" select="''"/>


<xsl:template match="set|book|part|preface|chapter|appendix|article
                      |reference|refentry
                      |book/glossary|article/glossary|part/glossary
                      |book/bibliography|article/bibliography|part/bibliography
                      |colophon">
  <xsl:call-template name="conditional-apply-imports"/>
</xsl:template>

<xsl:template match="sect1|sect2|sect3|sect4|sect5|section">
  <xsl:call-template name="conditional-apply-imports"/>
</xsl:template>

<xsl:template match="setindex
                      |book/index
                      |article/index
                      |part/index">
  <xsl:call-template name="conditional-apply-imports"/>
</xsl:template>

<xsl:template name="conditional-apply-imports">
  <xsl:if test="descendant-or-self::*[@id=$chunk.id]
                 | following-sibling::*[@id=$chunk.id]
                 | preceding-sibling::*[@id=$chunk.id]">
    <xsl:apply-imports/>
  </xsl:if>
</xsl:template>


</xsl:stylesheet>
